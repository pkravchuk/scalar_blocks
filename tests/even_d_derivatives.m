(* ::Package:: *)

ClearAll[prederivativeTable2d];
prederivativeTable2d[nmax_,Delta_,L_,Delta12_,Delta34_]:=prederivativeTable2d[nmax,Delta,L,Delta12,Delta34]=Module[{zder,zbder},
Dispatch@Flatten@Table[
zzb[zder,zbder]->1/2 Pochhammer[(Delta+L)/2-Delta12/2,zder]Pochhammer[(Delta+L)/2+Delta34/2,zder]Pochhammer[(Delta-L)/2-Delta12/2,zbder]Pochhammer[(Delta-L)/2+Delta34/2,zbder]Hypergeometric2F1[(Delta+L)/2-Delta12/2+zder,(Delta+L)/2+Delta34/2+zder,Delta+L+zder,1/2]Hypergeometric2F1[(Delta-L)/2-Delta12/2+zbder,(Delta-L)/2+Delta34/2+zbder,Delta-L+zbder,1/2]/Pochhammer[Delta+L,zder]/Pochhammer[Delta-L,zbder]
,{zder,0,2*nmax-1},{zbder,0,2*nmax-1-zder}]
]


Dz[expr_,x_]:=x^2(1-x)D[expr,x,x]-(a+b+1)x^2 D[expr,x]-a*b*x*expr;
operatorEzzb=(z zb)/(z-zb) (Dz[f[z,zb],z]-Dz[f[z,zb],zb]);


operatorExt=(operatorEzzb/.{f->Function[{z,zb},f[(z+zb-1)/2,((z-zb)/2)^2]]}/.{z->1/2+x+y,zb->1/2+x-y}//Expand)/.{y^n_:>t^(n/2)};


xtDerivativeBasis[nmax_]:=Flatten@Table[Derivative[m,n][f][0,0],{m,0,2*nmax-1},{n,0,(2*nmax-1-m)/2}];
zzbDerivativeBasis[nmax_]:=Flatten@Table[Derivative[m,n][f][0,0],{m,0,2*nmax-1},{n,0,Min[m,2*nmax-1-m]}];


ClearAll[derivativesXTFromZZB]
derivativesXTFromZZB[nmax_]:=derivativesXTFromZZB[nmax]=Module[{M,tmp,\[Epsilon],tmpcoeffs,zzbders},
tmp=Normal@Series[f[(z+zb)/2,((z-zb)/2)^2]/.{z->\[Epsilon]*z,zb->\[Epsilon]*zb},{\[Epsilon],0,2nmax-1}]/.\[Epsilon]->1;
tmpcoeffs=CoefficientList[tmp,{z,zb}];
zzbders=zzbDerivativeBasis[nmax]/.{f[0,0]->zzb[0,0],Derivative[a__][_][__]:>zzb[a]};
tmp=zzbders/.{zzb[m_,n_]:>m!n!tmpcoeffs[[m+1,n+1]]};
M=Coefficient[#,xtDerivativeBasis[nmax]]&/@Expand[tmp];
Inner[Rule,xtDerivativeBasis[nmax],Inverse[M].zzbDerivativeBasis[nmax],List]
]


ClearAll[operatorE];
operatorE[nmax_,zder_,zbder_]:=operatorE[_,zder,zbder]=Expand[zder!*zbder!*SeriesCoefficient[operatorExt/.{x->(z+zb-1)/2,t->((z-zb)/2)^2},{z,1/2,zder},{zb,1/2,zbder}]/.derivativesXTFromZZB[nmax]];


ClearAll[derivativeTable];
derivativeTable[nmax_,Delta_,L_,Delta12_,Delta34_,2]:=derivativeTable[nmax,Delta,L,Delta12,Delta34,2]=Module[
{tmp,f,zder,zbder},
tmp=4^(-Delta)(z^((Delta+L)/2) zb^((Delta-L)/2) f[z,zb]+zb^((Delta+L)/2) z^((Delta-L)/2) f[zb,z]);
Flatten@Table[
zzbDeriv[zder,zbder]->D[tmp,{z,zder},{zb,zbder}]/.{z->1/2,zb->1/2}
,{zder,0,2*nmax-1},{zbder,0,Min[2*nmax-1-zder,zder]}]/.{Derivative[m_,n_][f][__]:>zzb[m,n],f[__]:>zzb[0,0]}/.prederivativeTable2d[nmax,Delta,L,Delta12,Delta34]
]

derivativeTable[nmax_,Delta_,L_,Delta12_,Delta34_,d_]/;EvenQ[d]&&d>2 := derivativeTable[nmax,Delta,L,Delta12,Delta34,d]=Module[
{dold=d-2,Lold=L+1,Deltaold=Delta-1,tmp,zder,zbder},
Flatten@Table[
zzbDeriv0[zder,zbder]->Expand[((dold-1))/(2(Deltaold-1)Lold(Lold+dold-2)) operatorE[nmax+2,zder,zbder]/.{a->-Delta12/2,b->Delta34/2}]
,{zder,0,2*nmax-1},{zbder,0,Min[zder,2*nmax-1-zder]}]/.{Derivative[m_,n_][f][__]:>zzbDeriv[m,n],f[__]:>zzbDeriv[0,0]}/.
Dispatch@derivativeTable[nmax+2,Deltaold,Lold,Delta12,Delta34,dold]/.zzbDeriv0->zzbDeriv
]


(* The above code was tested against the analytic expressions for d=2,4,6 *)


SetDirectory[FileNameJoin[{NotebookDirectory[],"output-2"}]];


Delta12code=0.8791`200;
Delta34code=1.5791`200;
kPOcode=20;


DeltaTest=3.124151`200;


getCodeOutput[dim_,L_]:=Import["zzbDerivTable-d"<>ToString[dim]<>"-delta12-0.8791-delta34-1.5791-L"<>ToString[L]<>"-nmax6-keptPoleOrder20-order60.m"];


cbPoleProduct[order_,dim_,L_]:=Module[{nu=(dim-2)/2},Product[(Delta-(1-L-k))^If[2*nu+2*L+2*k<=order,2,1],{k,1,order}]*Product[Delta-(1+nu-k),{k,1,Min[nu-1,order/2]}]*Product[If[k>L&&k<L+2nu,1,Delta-(1+2nu+L-k)],{k,1,Min[2L+2nu,order]}]]


cbPoleProductWithR[Deltaval_][order_,dim_,L_]:=(3-2Sqrt[2])^Deltaval/cbPoleProduct[order,dim,L]/.{Delta->Deltaval}


xRule[Delta_,dim_,L_]:={x->Delta-(L+dim-2)};


ClearAll[getCodeDerivativeTable]
getCodeDerivativeTable[Deltaval_,dim_,L_]:=getCodeDerivativeTable[Deltaval,dim,L]=(getCodeOutput[dim,L]/.{(a_->b_):>(a->(cbPoleProductWithR[Deltaval][kPOcode,dim,L]b/.xRule[Deltaval,dim,L]))})


Monitor[
Table[derivativeTable[6,DeltaTest+Lcurr,Lcurr,Delta12code,Delta34code,2]/.Rule[a_,b_]:>(a-b)/b/.getCodeDerivativeTable[DeltaTest+Lcurr,2,Lcurr]//Abs//Max//N,{Lcurr,0,10}]
,{Lcurr}]


Monitor[
Table[derivativeTable[6,DeltaTest+Lcurr+2,Lcurr,Delta12code,Delta34code,4]/.Rule[a_,b_]:>(a-b)/b/.getCodeDerivativeTable[DeltaTest+Lcurr+2,4,Lcurr]//Abs//Max//N,{Lcurr,0,10}]
,{Lcurr}]


Monitor[
Table[derivativeTable[4,DeltaTest+Lcurr+4,Lcurr,Delta12code,Delta34code,6]/.Rule[a_,b_]:>(a-b)/b/.getCodeDerivativeTable[DeltaTest+Lcurr+4,6,Lcurr]//Abs//Max//N,{Lcurr,0,10}]
,{Lcurr}]


Monitor[
Table[derivativeTable[2,DeltaTest+Lcurr+6,Lcurr,Delta12code,Delta34code,8]/.Rule[a_,b_]:>(a-b)/b/.getCodeDerivativeTable[DeltaTest+Lcurr+6,8,Lcurr]//Abs//Max//N,{Lcurr,0,10}]
,{Lcurr}]
