#pragma once

#include <boost/math/constants/constants.hpp>
#include <boost/math/special_functions.hpp>
#include <boost/multiprecision/gmp.hpp>

using Bigfloat = boost::multiprecision::mpf_float;

// We implement factorial,binomial, and Pochhammer by hand because
// boost::math's versions want to initialized epsilon in a static
// constructor.  However, Bigfloat has not had its precision set yet,
// so it ends up dividing by zero and crashing.

inline Bigfloat factorial(const int64_t &n)
{
  Bigfloat result(1);
  for(int64_t kk = 2; kk <= n; ++kk)
    {
      result *= kk;
    }
  return result;
}

// Boost's implementation of rising_factorial is horrifically slow.
inline Bigfloat Pochhammer(const Bigfloat &alpha, const int64_t &n)
{
  Bigfloat result(1);
  for(int64_t kk = 0; kk < n; ++kk)
    {
      result *= alpha + kk;
    }
  return result;
}

inline Bigfloat binomial_coefficient(const int64_t &k, const int64_t &j)
{
  if(k > j)
    {
      return Pochhammer(k - j + 1, j) / factorial(j);
    }
  else
    {
      return Pochhammer(j - k + 1, k) / factorial(k);
    }
}

