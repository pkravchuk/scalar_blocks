#include "Timers.hxx"
#include "Bigfloat.hxx"
#include "Dimension/Dimension.hxx"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <vector>

namespace po = boost::program_options;

std::pair<std::set<int64_t>, int64_t>
parse_spin_ranges(const std::string &spin_ranges_string);

void write_zzb(const boost::filesystem::path &directory, const Dimension &dim,
               const std::string &Delta_12, const std::string &Delta_34,
               const std::pair<std::set<int64_t>, int64_t> &spins,
               const int64_t &n_max, const bool &output_ab,
               const std::vector<int64_t> &kept_pole_orders,
               const int64_t &order, const size_t &num_threads,
               const bool &debug, Timers &timers);

int main(int argc, char *argv[])
{
  try
    {
      po::options_description options("Scalar Blocks options");

      int64_t order, n_max;
      std::string spin_ranges_string;
      size_t num_threads;
      std::string Delta_12, Delta_34, dimstring;
      size_t precision_base_2;
      boost::filesystem::path output_directory;
      std::vector<int64_t> kept_pole_orders;
      bool debug(false);
      bool output_ab(false);

      options.add_options()("help,h", "Show this helpful message.")(
        "dim", po::value<std::string>(&dimstring)->required(), "Dimension")(
        "order", po::value<int64_t>(&order)->required(), "Depth of recursion")(
        "max-derivs", po::value<int64_t>(&n_max)->required(),
        "Max number of derivs (n_max)")(
        "output-ab", po::bool_switch(&output_ab), "Output a,b derivatvies "
        "instead of z,zb derivatives.")(
        "spin-ranges", po::value<std::string>(&spin_ranges_string)->required(),
        "Comma separated list of ranges of spins to compute "
        "(e.g. 12-16,22,37-49)")(
        "poles",
        po::value<std::vector<int64_t>>(&kept_pole_orders)->required(),
        "List of pole orders to keep")(
        "delta-12", po::value<std::string>(&Delta_12)->required(), "Δ 12")(
        "delta-34", po::value<std::string>(&Delta_34)->required(), "Δ 34")(
        "num-threads", po::value<size_t>(&num_threads)->required(),
        "Number of threads")("precision",
                             po::value<size_t>(&precision_base_2)->required(),
                             "The precision, in the number of bits, for "
                             "numbers in the computation.")(
        ",o",
        po::value<boost::filesystem::path>(&output_directory)->required(),
        "Output directory")("debug",
                            po::value<bool>(&debug)->default_value(false),
                            "Write out debugging output.");

      po::variables_map variables_map;

      po::store(po::parse_command_line(argc, argv, options), variables_map);
      if(variables_map.count("help"))
        {
          std::cout << options << '\n';
          return 0;
        }
      po::notify(variables_map);

      const size_t precision_base_10(
        boost::multiprecision::detail::digits2_2_10(precision_base_2));

      boost::multiprecision::mpf_float::default_precision(precision_base_10);

      const Bigfloat r_crossing(3 - 2 * sqrt(Bigfloat(2)));

      std::pair<std::set<int64_t>, int64_t> spins(
        parse_spin_ranges(spin_ranges_string));

      Dimension dim(dimstring);

      Timers timers(debug);
      Timer &total_timer(timers.add_and_start("Total"));
      write_zzb(output_directory, dim, Delta_12, Delta_34, spins, n_max,
                output_ab, kept_pole_orders, order, num_threads, debug, timers);
      total_timer.stop();
      if(debug)
        {
          timers.write_profile(output_directory / "profile");
        }
    }
  catch(std::exception &e)
    {
      std::cout << "Error: " << e.what() << "\n";
      exit(1);
    }
  catch(...)
    {
      std::cout << "Unknown error\n";
      exit(1);
    }
}
