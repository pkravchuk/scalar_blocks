#pragma once

#include <vector>
#include <utility>

std::vector<std::pair<int64_t,int64_t>> mn_deriv_set(const int64_t &nmax);
