#pragma once

#include <string>
#include "../Bigfloat.hxx"

struct Dimension
{
  Bigfloat nu;
  int64_t nu_int, dim_int;
  bool even;
  bool integer;
  std::string str;
  Dimension(const std::string &);
};