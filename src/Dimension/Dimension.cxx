#include "Dimension.hxx"

Dimension::Dimension(const std::string &dimstring)
    : even(false), integer(false), str(dimstring)
{
  Bigfloat dim;

  try
    {
      dim = Bigfloat(dimstring);
      nu = (dim - 2) / 2;
    }
  catch(const std::runtime_error &e)
    {
      throw std::runtime_error(
        "Dimension is not a valid floating-point number. Original exception:"
        + std::string(e.what()));
    };
  try
    {
      dim_int = std::stoi(dimstring);
      integer = dim == Bigfloat(dim_int);
      if(dim_int % 2 == 0 && integer)
        {
          even = true;
          nu_int = (dim_int - 2) / 2;
        }
      else
        nu_int = -1;
    }
  catch(const std::invalid_argument &e)
    {
      nu_int = -1;
    }
}