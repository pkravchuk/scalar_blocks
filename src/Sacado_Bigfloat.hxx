#pragma once

#include "Bigfloat.hxx"

// For reasons that are really unclear, this has to be defined before
// we #include Sacado
namespace std
{
  inline Bigfloat exp(const Bigfloat &m)
  {
    return boost::multiprecision::exp(m);
  }
  inline Bigfloat log(const Bigfloat &m)
  {
    return boost::multiprecision::log(m);
  }
  inline Bigfloat sqrt(const Bigfloat &m)
  {
    return boost::multiprecision::sqrt(m);
  }
}

#include <Sacado_No_Kokkos.hpp>
