#include "../../Sacado_Bigfloat.hxx"

std::vector<Bigfloat>
compute_h_infinity(const Bigfloat &nu,
                   const Bigfloat &Delta_12, const Bigfloat &Delta_34,
                   const int64_t &order)
{
  Sacado::Tay::Taylor<Bigfloat> r(order);
  r.fastAccessCoeff(0) = 0;
  r.fastAccessCoeff(1) = 1.0;
  Sacado::Tay::Taylor<Bigfloat> u = pow(1-r,-1+Delta_12-Delta_34-nu)
    * pow(1+r,-1-Delta_12+Delta_34-nu);
  std::vector<Bigfloat> result;
  result.reserve(u.degree()+1);
  for(int64_t deriv=0; deriv<=u.degree(); ++deriv)
    {
      result.emplace_back(u.coeff(deriv));
    }
  return result;
}
