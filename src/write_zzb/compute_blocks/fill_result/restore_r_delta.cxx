#include "../dot.hxx"
#include "../Matrix.hxx"
#include "../../Delta_Fraction.hxx"

#include <vector>
#include <utility>

std::vector<Delta_Fraction> restore_r_delta(
  const std::vector<Bigfloat> &partial_polynomial,
  const std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>
    &partial_residues,
  const std::vector<
    std::pair<Bigfloat, std::vector<std::pair<Bigfloat, Bigfloat>>>>
    &partial_double_residues,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &r_delta,
  const std::vector<std::pair<int64_t, int64_t>> &derivs)
{
  std::vector<std::vector<std::pair<boost::math::tools::polynomial<Bigfloat>,
                                    boost::math::tools::polynomial<Bigfloat>>>>
    pole_products;
  std::vector<
    std::vector<std::pair<boost::math::tools::polynomial<Bigfloat>,
                          std::pair<boost::math::tools::polynomial<Bigfloat>,
                                    boost::math::tools::polynomial<Bigfloat>>>>>
    double_pole_products;

  for(auto &residue : partial_residues)
    {
      pole_products.emplace_back();
      auto &pole_products_back(pole_products.back());
      for(size_t deriv_element(0); deriv_element < derivs.size();
          ++deriv_element)
        {
          boost::math::tools::polynomial<Bigfloat> r_dot_residue;
          for(size_t deriv_of_derivs(0); deriv_of_derivs < derivs.size();
              ++deriv_of_derivs)
            {
              r_dot_residue += r_delta[deriv_element][deriv_of_derivs]
                               * residue.second[deriv_of_derivs];
            }
          boost::math::tools::polynomial<Bigfloat> divisor(
            {-residue.first, 1});
          pole_products_back.push_back(
            boost::math::tools::quotient_remainder(r_dot_residue, divisor));
        }
    }

  for(auto &residue : partial_double_residues)
    {
      double_pole_products.emplace_back();
      auto &double_pole_products_back(double_pole_products.back());
      for(size_t deriv_element(0); deriv_element < derivs.size();
          ++deriv_element)
        {
          boost::math::tools::polynomial<Bigfloat> r_dot_residue;
          boost::math::tools::polynomial<Bigfloat> r_dot_double_residue;
          for(size_t deriv_of_derivs(0); deriv_of_derivs < derivs.size();
              ++deriv_of_derivs)
            {
              r_dot_residue += r_delta[deriv_element][deriv_of_derivs]
                               * residue.second[deriv_of_derivs].first;
              r_dot_double_residue += r_delta[deriv_element][deriv_of_derivs]
                                      * residue.second[deriv_of_derivs].second;
            }
          boost::math::tools::polynomial<Bigfloat> divisor(
            {-residue.first, 1});
          auto first_qr(
            boost::math::tools::quotient_remainder(r_dot_residue, divisor));
          auto second_qr(boost::math::tools::quotient_remainder(
            r_dot_double_residue, divisor * divisor));
          auto third_qr(
            boost::math::tools::quotient_remainder(second_qr.second, divisor));
          double_pole_products_back.emplace_back(
            first_qr.first + second_qr.first,
            std::make_pair(first_qr.second + third_qr.first,
                           third_qr.second));
        }
    }

  std::vector<Delta_Fraction> result;
  for(size_t deriv_element(0); deriv_element < derivs.size(); ++deriv_element)
    {
      result.emplace_back();
      auto &polynomial(result.back().polynomial);
      for(size_t deriv_of_derivs(0); deriv_of_derivs < derivs.size();
          ++deriv_of_derivs)
        {
          polynomial += r_delta[deriv_element][deriv_of_derivs]
                        * partial_polynomial[deriv_of_derivs];
        }
      for(auto &pole_product : pole_products)
        {
          polynomial += pole_product[deriv_element].first;
        }
      for(auto &double_pole_product : double_pole_products)
        {
          polynomial += double_pole_product[deriv_element].first;
        }


      auto &residues(result.back().residues);
      for(size_t pole(0); pole < pole_products.size(); ++pole)
        {
          // If the remainder is zero, the remainder is empty.  So
          // need to make sure not to access an empty element.
          residues.emplace_back(
            partial_residues[pole].first,
            pole_products[pole][deriv_element].second.size() == 0
              ? Bigfloat(0)
              : pole_products[pole][deriv_element].second[0]);
        }
      auto &double_residues(result.back().double_residues);
      for(size_t pole(0); pole < double_pole_products.size(); ++pole)
        {
          // If the remainder is zero, the remainder is empty.  So
          // need to make sure not to access an empty element.
          double_residues.emplace_back(
            partial_double_residues[pole].first,
            std::make_pair(
              double_pole_products[pole][deriv_element].second.first.size()
                  == 0
                ? Bigfloat(0)
                : double_pole_products[pole][deriv_element].second.first[0],
              double_pole_products[pole][deriv_element].second.second.size()
                  == 0
                ? Bigfloat(0)
                : double_pole_products[pole][deriv_element].second.second[0]));
        }
    }
  return result;
}

// std::vector<Delta_Fraction> restore_r_delta(
//   const std::vector<Bigfloat> &partial_polynomial,
//   const std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>
//     &partial_residues,
//   const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
//     &r_delta,
//   const std::vector<std::pair<int64_t, int64_t>> &derivs)
// {
//   std::vector<std::vector<std::pair<boost::math::tools::polynomial<Bigfloat>,
//                                     boost::math::tools::polynomial<Bigfloat>>>>
//     pole_products;
//   for(auto &residue : partial_residues)
//     {
//       pole_products.emplace_back();
//       auto &pole_products_back(pole_products.back());
//       for(size_t deriv_element(0); deriv_element < derivs.size();
//           ++deriv_element)
//         {
//           boost::math::tools::polynomial<Bigfloat> r_dot_residue;
//           for(size_t deriv_of_derivs(0); deriv_of_derivs < derivs.size();
//               ++deriv_of_derivs)
//             {
//               r_dot_residue += r_delta[deriv_element][deriv_of_derivs]
//                                * residue.second[deriv_of_derivs];
//             }
//           boost::math::tools::polynomial<Bigfloat> divisor(
//             {-residue.first, 1});
//           pole_products_back.push_back(
//             boost::math::tools::quotient_remainder(r_dot_residue, divisor));
//         }
//     }

//   std::vector<Delta_Fraction> result;
//   for(size_t deriv_element(0); deriv_element < derivs.size();
//   ++deriv_element)
//     {
//       result.emplace_back();
//       auto &polynomial(result.back().polynomial);
//       for(size_t deriv_of_derivs(0); deriv_of_derivs < derivs.size();
//           ++deriv_of_derivs)
//         {
//           polynomial += r_delta[deriv_element][deriv_of_derivs]
//                         * partial_polynomial[deriv_of_derivs];
//         }
//       for(auto &pole_product : pole_products)
//         {
//           polynomial += pole_product[deriv_element].first;
//         }

//       auto &residues(result.back().residues);
//       for(size_t pole(0); pole < pole_products.size(); ++pole)
//         {
//           // If the remainder is zero, the remainder is empty.  So
//           // need to make sure not to access an empty element.
//           residues.emplace_back(
//             partial_residues[pole].first,
//             pole_products[pole][deriv_element].second.size() == 0
//               ? Bigfloat(0)
//               : pole_products[pole][deriv_element].second[0]);
//         }
//     }
//   return result;
// }
