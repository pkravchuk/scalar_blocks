#include "../Residues.hxx"
#include "../Matrix.hxx"
#include "../../Delta_Fraction.hxx"
#include "../../../Timers.hxx"

// std::vector<Delta_Fraction> restore_r_delta(
//   const std::vector<Bigfloat> &partial_polynomial,
//   const std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>
//     &partial_residues,
//   const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
//     &r_delta,
//   const std::vector<std::pair<int64_t, int64_t>> &derivs);

std::vector<Delta_Fraction> restore_r_delta(
  const std::vector<Bigfloat> &partial_polynomial,
  const std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>
    &partial_residues,
  const std::vector<
    std::pair<Bigfloat, std::vector<std::pair<Bigfloat, Bigfloat>>>>
    &partial_double_residues,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &r_delta,
  const std::vector<std::pair<int64_t, int64_t>> &derivs);

void fill_result(
  const std::vector<Bigfloat> &partial_polynomial, const int64_t &L_max,
  const Bigfloat &nu, const Residues &residues,
  const std::vector<std::pair<int64_t, int64_t>> &derivs,
  const Matrix<Bigfloat> &derivs_around_rho,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &r_delta,
  const size_t &num_threads, const size_t &thread_rank,
  std::vector<std::vector<Delta_Fraction>> &result, Timer &thread_timer)
{
  for(int64_t L = thread_rank; L <= L_max; L += num_threads)
    {
      std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>> partial_residues;
      std::vector<
        std::pair<Bigfloat, std::vector<std::pair<Bigfloat, Bigfloat>>>>
        double_partial_residues(0);

      for(int64_t o = 0;
          o < static_cast<int64_t>(residues.alpha[L].front().size()); ++o)
        {
          partial_residues.emplace_back(Bigfloat(1 - L - (o + 1)),
                                        std::vector<Bigfloat>());
          auto &residue(partial_residues.back().second);
          for(size_t n = 0; n < derivs_around_rho.row_size; ++n)
            {
              residue.emplace_back(0);
              auto &pole(residue.back());
              for(size_t m = 0; m < derivs_around_rho.column_size; ++m)
                {
                  pole += residues.alpha[L][m][o] * derivs_around_rho(n, m);
                }
            }
        }
      for(int64_t o = 0;
          o < static_cast<int64_t>(residues.beta[L].front().size()); ++o)
        {
          partial_residues.emplace_back(Bigfloat(1 + nu - (o + 1)),
                                        std::vector<Bigfloat>());
          auto &residue(partial_residues.back().second);
          for(size_t n = 0; n < derivs_around_rho.row_size; ++n)
            {
              residue.emplace_back(0);
              auto &pole(residue.back());
              for(size_t m = 0; m < derivs_around_rho.column_size; ++m)
                {
                  pole += residues.beta[L][m][o] * derivs_around_rho(n, m);
                }
            }
        }
      for(int64_t o = 0;
          o < static_cast<int64_t>(residues.gamma[L].front().size()); ++o)
        {
          partial_residues.emplace_back(Bigfloat(L + 2 * nu + 1 - (o + 1)),
                                        std::vector<Bigfloat>());
          auto &residue(partial_residues.back().second);
          for(size_t n = 0; n < derivs_around_rho.row_size; ++n)
            {
              residue.emplace_back(0);
              auto &pole(residue.back());
              for(size_t m = 0; m < derivs_around_rho.column_size; ++m)
                {
                  pole += residues.gamma[L][m][o] * derivs_around_rho(n, m);
                }
            }
        }
      result[L] = restore_r_delta(partial_polynomial, partial_residues,
                                  double_partial_residues, r_delta, derivs);
    }
  thread_timer.stop();
}
