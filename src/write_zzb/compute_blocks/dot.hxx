#pragma once

#include "Matrix.hxx"

#include <stdexcept>
#include <string>

template <typename T> Matrix<T> dot(const Matrix<T> &a, const Matrix<T> &b)
{
  Matrix<T> result(a.row_size, b.column_size);
  for(size_t a_row = 0; a_row < a.row_size; ++a_row)
    for(size_t b_column = 0; b_column < a.column_size; ++b_column)
      {
        result(a_row, b_column) = 0;
        for(size_t element = 0; element < a.column_size; ++element)
          {
            result(a_row, b_column)
              += a(a_row, element) * b(element, b_column);
          }
      }
  return result;
}

// FIXME: This should use something like decltype(a(0,0)*b[0])
template <typename T1, typename T2>
std::vector<T1> dot(const Matrix<T1> &a, const std::vector<T2> &b)
{
  if(a.column_size != b.size())
    {
      throw std::runtime_error(
        "Incompatible sizes in dot(Matrix,vector).  Found Matrix("
        + std::to_string(a.row_size) + "," + std::to_string(a.column_size)
        + ") vector(" + std::to_string(b.size()) + ")");
    }
  std::vector<T1> result(a.row_size, T1(0));
  for(size_t a_row = 0; a_row < a.row_size; ++a_row)
    for(size_t element = 0; element < a.column_size; ++element)
      {
        result[a_row] += a(a_row, element) * b[element];
      }
  return result;
}

template <typename T> T dot(const std::vector<T> &a, const std::vector<T> &b)
{
  T result(0);
  if(a.size() != b.size())
    {
      throw std::runtime_error(
        "INTERNAL ERROR: In dot(), different sizes for a and b: "
        + std::to_string(a.size()) + " != " + std::to_string(b.size()));
    }
  for(size_t index = 0; index < a.size(); ++index)
    {
      result += a[index] * b[index];
    }
  return result;
}
