#include "../../../Residues.hxx"

#include <numeric>
#include <thread>

namespace
{
  Bigfloat alpha(const Bigfloat &Delta_12, const Bigfloat &Delta_34,
                 const Bigfloat &nu, const int64_t &n, const int64_t &L)
  {
    return -((pow(Bigfloat(4), n) * n)
             / pow(factorial(n), 2))
           * Pochhammer(0.5 * (1 - n + Delta_12), n)
           * Pochhammer(0.5 * (1 - n + Delta_34), n)
           * Pochhammer(2 * nu + L, n) / Pochhammer(nu + L, n);
  }

  Bigfloat beta(const Bigfloat &Delta_12, const Bigfloat &Delta_34,
                const Bigfloat &nu, const int64_t &k, const int64_t &L)
  {
    return -(pow(Bigfloat(4), 2 * k) * k * pow(-1, k)
             * Pochhammer(-k + nu, 2 * k)
             * Pochhammer(0.5 * (1 - k + L - Delta_12 + nu), k)
             * Pochhammer(0.5 * (1 - k + L + Delta_12 + nu), k)
             * Pochhammer(0.5 * (1 - k + L - Delta_34 + nu), k)
             * Pochhammer(0.5 * (1 - k + L + Delta_34 + nu), k))
           / (pow(factorial(k), 2.0)
              * Pochhammer(-k + L + nu, 2 * k)
              * Pochhammer(1 - k + L + nu, 2 * k));
  }

  Bigfloat gamma(const Bigfloat &Delta_12, const Bigfloat &Delta_34,
                 const Bigfloat &nu, const int64_t &n, const int64_t &L)
  {
    return -((pow(4, n) * n) / pow(factorial(n), 2))
           * Pochhammer(0.5 * (1 - n + Delta_12), n)
           * Pochhammer(0.5 * (1 - n + Delta_34), n) * Pochhammer(1 + L - n, n)
           / Pochhammer(1 + nu + L - n, n);
  }
}

Bigfloat
h_dot(const std::vector<Bigfloat> &h_infinity, const Residues &residues,
      const Bigfloat &nu, const int64_t &recursion_level, const int64_t &rL,
      const Bigfloat &rDelta, const int64_t &rShift);

void append_residues(const std::vector<Bigfloat> &h_infinity,
                     const int64_t &order, const Bigfloat &nu,
                     const Bigfloat &Delta_12, const Bigfloat &Delta_34,
                     const int64_t &L_max_recursion,
                     const int64_t &recursion_level, const size_t &num_threads,
                     const size_t &thread_rank,
                     const std::vector<uint64_t> &timings, Residues &residues,
                     std::vector<uint64_t> &new_timings,
                     Timer &thread_timer)
{
  const int64_t L_iteration_max(L_max_recursion - recursion_level + 1);
  if(L_iteration_max > static_cast<int64_t>(timings.size()))
    {
      throw std::runtime_error(
        "INTERNAL ERROR: L_iteration_max > timing.size(): "
        "L_iteration_max: "
        + std::to_string(L_iteration_max)
        + " timings.size(): " + std::to_string(timings.size()));
    }
  
  // Should think about start_time, end_time and current_time (below) as 
  // numerators, with denominator num_threads.
  const uint64_t total_time(std::accumulate(
    timings.begin(), timings.begin() + L_iteration_max,
    int64_t(0)));
  uint64_t start_time(thread_rank * total_time),
    end_time((thread_rank + 1) * total_time);

  uint64_t current_time(0);
  int64_t L_begin(L_iteration_max), L_end(L_iteration_max);
  for(int64_t L = 0; L < L_iteration_max; ++L)
    {
      current_time += timings[L] * num_threads;
      if(L_begin == L_iteration_max && current_time >= start_time)
        {
          L_begin = L;
        }
      if(L_end == L_iteration_max && current_time > end_time)
        {
          L_end = L;
        }
    }

  for(int64_t L = L_begin; L < L_end; ++L)
    {
      Timer block_timer;
      const int64_t max_pole_order(std::min(order, L_max_recursion - L));
      residues.alpha.at(L).at(recursion_level).resize(max_pole_order, 0);
      residues.beta.at(L).at(recursion_level).resize(max_pole_order / 2, 0);
      residues.gamma.at(L)
        .at(recursion_level)
        .resize(std::min(max_pole_order, L), 0);

      for(int64_t n = 1; n <= max_pole_order && n <= recursion_level; ++n)
        {
          const int64_t rShift(n), rL(L + n);
          Bigfloat rDelta((1 - L - n) + rShift),
            ralpha(alpha(Delta_12, Delta_34, nu, n, L));

          residues.alpha.at(L).at(recursion_level).at(n - 1)
            = ralpha
              * h_dot(h_infinity, residues, nu, recursion_level, rL, rDelta,
                      rShift);
        }

      for(int64_t k = 1; k <= max_pole_order / 2; ++k)
        {
          const int64_t n(2 * k);
          if(n <= recursion_level)
            {
              const int64_t rShift(n), rL(L);
              Bigfloat rDelta((1 + nu - k) + rShift),
                rbeta(beta(Delta_12, Delta_34, nu, k, L));

              residues.beta.at(L).at(recursion_level).at(k - 1)
                = rbeta
                  * h_dot(h_infinity, residues, nu, recursion_level, rL,
                          rDelta, rShift);
            }
        }

      for(int64_t n = 1;
          n <= std::min(max_pole_order, L) && n <= recursion_level; ++n)
        {
          const int64_t rShift(n), rL(L - n);
          Bigfloat rDelta((L + 2 * nu + 1 - n) + rShift),
            rgamma(gamma(Delta_12, Delta_34, nu, n, L));

          residues.gamma.at(L).at(recursion_level).at(n - 1)
            = rgamma
              * h_dot(h_infinity, residues, nu, recursion_level, rL, rDelta,
                      rShift);
        }
      block_timer.stop();
      new_timings.at(L) = block_timer.elapsed_microseconds();
    }
  thread_timer.stop();
}
