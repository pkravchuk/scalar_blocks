#include "../../../Residues.hxx"

#include <numeric>
#include <thread>

namespace
{
  int64_t parity(const int64_t &n) { return 1 - 2 * (n % 2); }

  Bigfloat b1(const int64_t &nu, const int64_t &k, const int64_t &L)
  {
    if(nu < 2)
      {
        return 0;
      }
    Bigfloat result(0);
    for(int64_t m = 1; m < nu; m++)
      {
        result += (Bigfloat(k) * nu * (2 * L + k + 2 * m + nu))
                  / (Bigfloat(L + m) * (L + k + m) * (L + m + nu)
                     * (L + k + m + nu));
      }
    return result;
  }

  Bigfloat a14(const Bigfloat &Delta_12, const Bigfloat &Delta_34,
               const int64_t &nu, const int64_t &k, const int64_t &L)
  {
    return parity(nu) * pow(Bigfloat(4), 2 * (L + k + nu))
           * Pochhammer(L + k + nu, nu)
           * Pochhammer((1 - k + Delta_12) / 2, nu + L + k)
           * Pochhammer((1 - k - Delta_12) / 2, nu + L + k)
           * Pochhammer((1 - k + Delta_34) / 2, nu + L + k)
           * Pochhammer((1 - k - Delta_34) / 2, nu + L + k)
           / (k * pow(factorial(k - 1), 2) * Pochhammer(1 + L + k, nu)
              * pow(factorial(2 * (L + nu) + k - 1), 2) * (2 * (L + nu) + k));
  }

  Bigfloat a13(const Bigfloat &Delta_12, const Bigfloat &Delta_34,
               const int64_t &nu, const int64_t &k, const int64_t &L)
  {
    if(nu < 2)
      {
        return 0;
      }
    return -a14(Delta_12, Delta_34, nu, k, L) * b1(nu, k, L);
  }

  Bigfloat a12(const Bigfloat &Delta_12, const Bigfloat &Delta_34,
               const int64_t &nu, const int64_t &k, const int64_t &L)
  {
    return parity(nu + 1) * pow(Bigfloat(4), 2 * (L + nu) + k)
           * Pochhammer(L + k + nu, nu)
           * Pochhammer((1 - k + Delta_12) / 2 - L - nu, 2 * (L + nu) + k)
           * Pochhammer((1 - k + Delta_34) / 2 - L - nu, 2 * (L + nu) + k)
           / (Pochhammer(L + 1, nu) * pow(factorial(2 * (L + nu) + k - 1), 2)
              * (2 * (L + nu) + k));
  }

  Bigfloat a11(const Bigfloat &Delta_12, const Bigfloat &Delta_34,
               const int64_t &nu, const int64_t &k, const int64_t &L)
  {
    return -pow(Bigfloat(4), k) * k * Pochhammer(L + k + nu, nu)
           * Pochhammer((1 - k + Delta_12) / 2, k)
           * Pochhammer((1 - k + Delta_34) / 2, k)
           / (pow(factorial(k), 2) * Pochhammer(L + nu, nu));
  }

  Bigfloat a2(const Bigfloat &Delta_12, const Bigfloat &Delta_34,
              const int64_t &nu, const int64_t &k, const int64_t &L)
  {
    return -(pow(Bigfloat(4), 2 * k) * k * parity(k)
             * Pochhammer(-k + nu, 2 * k)
             * Pochhammer(0.5 * (1 - k + L - Delta_12 + nu), k)
             * Pochhammer(0.5 * (1 - k + L + Delta_12 + nu), k)
             * Pochhammer(0.5 * (1 - k + L - Delta_34 + nu), k)
             * Pochhammer(0.5 * (1 - k + L + Delta_34 + nu), k))
           / (pow(factorial(k), 2) * Pochhammer(-k + L + nu, 2 * k)
              * Pochhammer(1 - k + L + nu, 2 * k));
  }

  Bigfloat a3(const Bigfloat &Delta_12, const Bigfloat &Delta_34,
              const int64_t &nu, const int64_t &k, const int64_t &L)
  {
    return -((pow(4, k) * k) / pow(factorial(k), 2))
           * Pochhammer(0.5 * (1 - k + Delta_12), k)
           * Pochhammer(0.5 * (1 - k + Delta_34), k)
           * Pochhammer(1 + L - k, nu) / Pochhammer(1 + L, nu);
  }
}

Bigfloat
h_val_even_d(const std::vector<Bigfloat> &h_infinity, const Residues &residues,
             const int64_t &nu, const int64_t &recursion_level,
             const int64_t &rL, const Bigfloat &rDelta, const int64_t &rShift);

Bigfloat
h_der_even_d(const Residues &residues,
             const int64_t &nu, const int64_t &recursion_level,
             const int64_t &rL, const Bigfloat &rDelta, const int64_t &rShift);

Bigfloat
h_reg_even_d(const std::vector<Bigfloat> &h_infinity, const Residues &residues,
             const int64_t &nu, const int64_t &recursion_level,
             const int64_t &rL, const int64_t &rDelta, const int64_t &rShift);

void process_single_spin(const std::vector<Bigfloat> &h_infinity,
                         const int64_t &order, const int64_t &nu,
                         const Bigfloat &Delta_12, const Bigfloat &Delta_34,
                         const int64_t &L_max_recursion,
                         const int64_t &recursion_level, Residues &residues,
                         const int64_t &L)
{
  const int64_t max_pole_order(std::min(order, L_max_recursion - L));
  // residues.alpha.at(L).at(recursion_level).resize(max_pole_order, 0);
  // residues.beta.at(L).at(recursion_level).resize(max_pole_order / 2, 0);
  // residues.gamma.at(L)
  //   .at(recursion_level)
  //   .resize(std::min(max_pole_order, L), 0);


  residues.alpha.at(L).at(recursion_level).resize(max_pole_order, 0);
  if(nu > 1)
    {
      residues.beta.at(L)
        .at(recursion_level)
        .resize(std::min(max_pole_order / 2, nu - 1), 0);
    }
  // For third family k=1...2L+2nu except k=J+1...J+2nu-1.
  // For these excluded values we will simply keep the entries as 0.
  residues.gamma.at(L)
    .at(recursion_level)
    .resize(std::min(max_pole_order, 2 * L + 2 * nu), 0);
  residues.alpha2.at(L)
    .at(recursion_level)
    .resize(std::max((max_pole_order - 2 * nu + L) / 2, int64_t(0)), 0);

 
  for(int64_t k = 1; k <= max_pole_order && k <= recursion_level; ++k)
    {
      int64_t rShift(k), rL(L + k);
      int64_t rDelta(1 - L);
      Bigfloat ra11(a11(Delta_12, Delta_34, nu, k, L));


      residues.alpha.at(L).at(recursion_level).at(k - 1)
        = ra11
          * h_reg_even_d(h_infinity, residues, nu, recursion_level, rL, rDelta,
                         rShift);


      rShift = 2 * nu + 2 * L + k;
      rDelta = 1 + 2 * nu + L;
      if(rShift <= max_pole_order && rShift <= recursion_level)
        {
          Bigfloat ra12(a12(Delta_12, Delta_34, nu, k, L));
          residues.alpha.at(L).at(recursion_level).at(k - 1)
            += ra12
               * h_reg_even_d(h_infinity, residues, nu, recursion_level, rL,
                              rDelta, rShift);
        }


      rShift = 2 * nu + 2 * L + 2 * k;
      rL = L;
      rDelta = 1 + 2 * nu + L + k;
      if(rShift <= max_pole_order && rShift <= recursion_level)
        {
          Bigfloat block(h_val_even_d(h_infinity, residues, nu,
                                      recursion_level, rL, rDelta, rShift));
          Bigfloat ra14 = a14(Delta_12, Delta_34, nu, k, L);
          Bigfloat ra13 = a13(Delta_12, Delta_34, nu, k, L);

          residues.alpha.at(L).at(recursion_level).at(k - 1)
            += ra13 * block
               - ra14
                   * h_der_even_d(residues, nu, recursion_level,
                                  rL, rDelta, rShift);

          residues.alpha2.at(L).at(recursion_level).at(k - 1) = ra14 * block;
        }
    }


  for(int64_t k = 1;
      k <= max_pole_order && k <= recursion_level && k <= 2 * L + 2 * nu; ++k)
    {
      if(k > L && k < L + 2 * nu)
        {
          continue;
        }

      int64_t rShift(k), rL;
      if(L >= k)
        {
          rL = L - k;
        }
      else
        {
          rL = k - 2 * nu - L;
        }

      Bigfloat rDelta(1 + 2 * nu + L), ra3(a3(Delta_12, Delta_34, nu, k, L));

      residues.gamma.at(L).at(recursion_level).at(k - 1)
        = ra3
          * h_val_even_d(h_infinity, residues, nu, recursion_level, rL, rDelta,
                         rShift);
    }


  if(nu > 1)
    {
      for(int64_t k = 1;
          k <= nu - 1 && 2 * k <= max_pole_order && 2 * k <= recursion_level;
          ++k)
        {
          const int64_t rShift(2 * k), rL(L);
          Bigfloat rDelta(1 + nu + k), ra2(a2(Delta_12, Delta_34, nu, k, L));

          residues.beta.at(L).at(recursion_level).at(k - 1)
            = ra2
              * h_val_even_d(h_infinity, residues, nu, recursion_level, rL,
                             rDelta, rShift);
        }
    }
}

void append_residues_even_d(
  const std::vector<Bigfloat> &h_infinity, const int64_t &order,
  const int64_t &nu, const Bigfloat &Delta_12, const Bigfloat &Delta_34,
  const int64_t &L_max_recursion, const int64_t &recursion_level,
  const size_t &num_threads, const size_t &thread_rank,
  const std::vector<uint64_t> &timings, Residues &residues,
  std::vector<uint64_t> &new_timings, Timer &thread_timer)
{
  // The range of spins we actually go over (over all threads) is
  // 0..L_iteration_max-1
  const int64_t L_iteration_max(L_max_recursion - recursion_level + 1);
  if(L_iteration_max > static_cast<int64_t>(timings.size()))
    {
      throw std::runtime_error(
        "INTERNAL ERROR: L_iteration_max > timing.size(): "
        "L_iteration_max: "
        + std::to_string(L_iteration_max)
        + " timings.size(): " + std::to_string(timings.size()));
    }

  // Should think about start_time, end_time and current_time (below) as
  // numerators, with denominator num_threads.
  const uint64_t total_time(std::accumulate(
    timings.begin(), timings.begin() + L_iteration_max, int64_t(0)));
  uint64_t start_time(thread_rank * total_time),
    end_time((thread_rank + 1) * total_time);

  uint64_t current_time(0);
  int64_t L_begin(L_iteration_max), L_end(L_iteration_max);
  for(int64_t L = 0; L < L_iteration_max; ++L)
    {
      current_time += timings[L] * num_threads;
      if(L_begin == L_iteration_max && current_time >= start_time)
        {
          L_begin = L;
        }
      if(L_end == L_iteration_max && current_time > end_time)
        {
          L_end = L;
        }
    }

  for(int64_t L = L_begin; L < L_end; ++L)
    {
      Timer block_timer;

      process_single_spin(h_infinity, order, nu, Delta_12, Delta_34,
                          L_max_recursion, recursion_level, residues, L);

      block_timer.stop();
      new_timings.at(L) = block_timer.elapsed_microseconds();
    }
  thread_timer.stop();
}
