#include "../../../Residues.hxx"
#include "../../../dot.hxx"

Bigfloat
h_val_even_d(const std::vector<Bigfloat> &h_infinity, const Residues &residues,
             const int64_t &nu, const int64_t &recursion_level,
             const int64_t &rL, const Bigfloat &rDelta, const int64_t &rShift)
{
  std::vector<Bigfloat> alpha_delta(
    residues.alpha.at(rL).at(recursion_level - rShift).size()),
    beta_delta(residues.beta.at(rL).at(recursion_level - rShift).size()),
    gamma_delta(residues.gamma.at(rL).at(recursion_level - rShift).size()),
    alpha2_delta(residues.alpha2.at(rL).at(recursion_level - rShift).size());

  for(int64_t n = 0; n < static_cast<int64_t>(alpha_delta.size()); ++n)
    {
      alpha_delta[n] = 1 / (rDelta - (1 - rL - (n + 1)));
    }
  for(int64_t n = 0; n < static_cast<int64_t>(alpha2_delta.size()); ++n)
    {
      alpha2_delta[n] = 1 / pow(rDelta - (1 - rL - (n + 1)), 2);
    }
  for(int64_t k = 0; k < static_cast<int64_t>(beta_delta.size()); ++k)
    {
      beta_delta[k] = 1 / (rDelta - (1 + nu - (k + 1)));
    }
  for(int64_t n = 0; n < static_cast<int64_t>(gamma_delta.size()); ++n)
    {
      if(n + 1 > rL && n + 1 < rL + 2 * nu)
        gamma_delta[n] = 0;
      else
        gamma_delta[n] = 1 / (rDelta - (rL + 2 * nu + 1 - (n + 1)));
    }
  return h_infinity.at(recursion_level - rShift)
         + dot(residues.alpha.at(rL).at(recursion_level - rShift), alpha_delta)
         + dot(residues.alpha2.at(rL).at(recursion_level - rShift),
               alpha2_delta)
         + dot(residues.beta.at(rL).at(recursion_level - rShift), beta_delta)
         + dot(residues.gamma.at(rL).at(recursion_level - rShift),
               gamma_delta);
}

Bigfloat
h_der_even_d(const Residues &residues,
             const int64_t &nu, const int64_t &recursion_level,
             const int64_t &rL, const Bigfloat &rDelta, const int64_t &rShift)
{
  std::vector<Bigfloat> alpha_delta(
    residues.alpha.at(rL).at(recursion_level - rShift).size()),
    beta_delta(residues.beta.at(rL).at(recursion_level - rShift).size()),
    gamma_delta(residues.gamma.at(rL).at(recursion_level - rShift).size()),
    alpha2_delta(residues.alpha2.at(rL).at(recursion_level - rShift).size());

  for(int64_t n = 0; n < static_cast<int64_t>(alpha_delta.size()); ++n)
    {
      alpha_delta[n] = -1 / pow(rDelta - (1 - rL - (n + 1)), 2);
    }
  for(int64_t n = 0; n < static_cast<int64_t>(alpha2_delta.size()); ++n)
    {
      alpha2_delta[n] = -2 / pow(rDelta - (1 - rL - (n + 1)), 3);
    }
  for(int64_t k = 0; k < static_cast<int64_t>(beta_delta.size()); ++k)
    {
      beta_delta[k] = -1 / pow(rDelta - (1 + nu - (k + 1)), 2);
    }
  for(int64_t n = 0; n < static_cast<int64_t>(gamma_delta.size()); ++n)
    {
      gamma_delta[n] = -1 / pow(rDelta - (rL + 2 * nu + 1 - (n + 1)), 2);
    }
  return dot(residues.alpha.at(rL).at(recursion_level - rShift), alpha_delta)
         + dot(residues.alpha2.at(rL).at(recursion_level - rShift),
               alpha2_delta)
         + dot(residues.beta.at(rL).at(recursion_level - rShift), beta_delta)
         + dot(residues.gamma.at(rL).at(recursion_level - rShift),
               gamma_delta);
}

Bigfloat
h_reg_even_d(const std::vector<Bigfloat> &h_infinity, const Residues &residues,
             const int64_t &nu, const int64_t &recursion_level,
             const int64_t &rL, const int64_t &rDeltaI, const int64_t &rShift)
{
  std::vector<Bigfloat> alpha_delta(
    residues.alpha.at(rL).at(recursion_level - rShift).size()),
    beta_delta(residues.beta.at(rL).at(recursion_level - rShift).size()),
    gamma_delta(residues.gamma.at(rL).at(recursion_level - rShift).size()),
    alpha2_delta(residues.alpha2.at(rL).at(recursion_level - rShift).size());

  Bigfloat rDelta(rDeltaI);
  for(int64_t n = 0; n < static_cast<int64_t>(alpha_delta.size()); ++n)
    {
      if(rDeltaI == 1 - rL - (n + 1))
        {
          alpha_delta[n] = 0;
          continue;
        }
      alpha_delta[n] = 1 / (rDelta - (1 - rL - (n + 1)));
    }
  for(int64_t n = 0; n < static_cast<int64_t>(alpha2_delta.size()); ++n)
    {
      if(rDeltaI == 1 - rL - (n + 1))
        {
          alpha2_delta[n] = 0;
          continue;
        }
      alpha2_delta[n] = 1 / pow(rDelta - (1 - rL - (n + 1)), 2);
    }
  for(int64_t k = 0; k < static_cast<int64_t>(beta_delta.size()); ++k)
    {
      if(rDeltaI == 1 + nu - (k + 1))
        {
          beta_delta[k] = 0;
          continue;
        }
      beta_delta[k] = 1 / (rDelta - (1 + nu - (k + 1)));
    }
  for(int64_t n = 0; n < static_cast<int64_t>(gamma_delta.size()); ++n)
    {
      if(rDeltaI == rL + 2 * nu + 1 - (n + 1))
        {
          gamma_delta[n] = 0;
          continue;
        }
      gamma_delta[n] = 1 / (rDelta - (rL + 2 * nu + 1 - (n + 1)));
    }
  return h_infinity.at(recursion_level - rShift)
         + dot(residues.alpha.at(rL).at(recursion_level - rShift), alpha_delta)
         + dot(residues.alpha2.at(rL).at(recursion_level - rShift),
               alpha2_delta)
         + dot(residues.beta.at(rL).at(recursion_level - rShift), beta_delta)
         + dot(residues.gamma.at(rL).at(recursion_level - rShift),
               gamma_delta);
}