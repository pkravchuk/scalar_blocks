#include "../../Residues.hxx"

#include <list>
#include <thread>

void append_residues(const std::vector<Bigfloat> &h_infinity,
                     const int64_t &order, const Bigfloat &nu,
                     const Bigfloat &Delta_12, const Bigfloat &Delta_34,
                     const int64_t &L_max_recursion,
                     const int64_t &recursion_level, const size_t &num_threads,
                     const size_t &thread_rank,
                     const std::vector<uint64_t> &timings, Residues &residues,
                     std::vector<uint64_t> &new_timings, Timer &thread_timer);

void append_residues_even_d(
  const std::vector<Bigfloat> &h_infinity, const int64_t &order,
  const int64_t &nu, const Bigfloat &Delta_12, const Bigfloat &Delta_34,
  const int64_t &L_max_recursion, const int64_t &recursion_level,
  const size_t &num_threads, const size_t &thread_rank,
  const std::vector<uint64_t> &timings, Residues &residues,
  std::vector<uint64_t> &new_timings, Timer &thread_timer);

Residues::Residues(std::vector<Bigfloat> &h_infinity,
                   const int64_t &L_max_recursion, const int64_t &order,
                   const Bigfloat &nu, const Bigfloat &Delta_12,
                   const Bigfloat &Delta_34, const size_t &num_threads,
                   const std::string &timer_prefix, Timers &timers)
    // FIXME: This really only needs to be L_max_recursion, L_max_recursion+1
    : alpha(L_max_recursion + 1), beta(L_max_recursion + 1),
      gamma(L_max_recursion + 1), alpha2(0)
{
  for(size_t L = 0; L < alpha.size(); ++L)
    {
      alpha[L].resize(order + 1);
      beta[L].resize(order + 1);
      gamma[L].resize(order + 1);

      // Set residues to zero at recursion_level==0
      const size_t max_pole_order(
        std::min<size_t>(order, L_max_recursion - L));
      alpha[L][0].resize(max_pole_order, 0);
      beta[L][0].resize(max_pole_order / 2, 0);
      gamma[L][0].resize(std::min(max_pole_order, L), 0);
    }

  // At the beginning, we have no data, so set all blocks to the same
  // cost.
  std::vector<uint64_t> timings(L_max_recursion, 1), new_timings(timings);
  for(int64_t recursion_level = 1; recursion_level <= order; ++recursion_level)
    {
      const std::string recursion_timer_name(
        timer_prefix + "recursion_" + std::to_string(recursion_level));
      Timer &recursion_timer(timers.add_and_start(recursion_timer_name));

      // Timer &thread_timer(
      //   timers.add_and_start(recursion_timer_name + ".thread_0"));
      // append_residues(h_infinity, order, nu, Delta_12, Delta_34,
      //                 L_max_recursion, recursion_level,
      //                 // num_threads, thread_rank,
      //                 1, 0,
      //                 timings, *this, new_timings, thread_timer);

      std::list<std::thread> threads;
      for(size_t thread_rank = 0; thread_rank < num_threads; ++thread_rank)
        {
          Timer &thread_timer(timers.add_and_start(
            recursion_timer_name + ".thread_" + std::to_string(thread_rank)));

          threads.emplace_back(
            append_residues, std::cref(h_infinity), order, std::cref(nu),
            std::cref(Delta_12), std::cref(Delta_34), L_max_recursion,
            recursion_level, num_threads, thread_rank, std::cref(timings),
            std::ref(*this), std::ref(new_timings), std::ref(thread_timer));
        }
      for(auto &thread : threads)
        {
          thread.join();
        }
      recursion_timer.stop();
      std::swap(timings, new_timings);
    }
}

// Constructor for even-dimensional case
Residues::Residues(std::vector<Bigfloat> &h_infinity,
                   const int64_t &L_max_recursion, const int64_t &order,
                   const int64_t &nu, const Bigfloat &Delta_12,
                   const Bigfloat &Delta_34, const size_t &num_threads,
                   const std::string &timer_prefix, Timers &timers)
    // FIXME: This really only needs to be L_max_recursion, L_max_recursion+1
    // FIXME: alpha2 can be much shorter, as well as beta for nu<2
    : alpha(L_max_recursion + 1), beta(L_max_recursion + 1),
      gamma(L_max_recursion + 1), alpha2(L_max_recursion + 1)
{
  for(size_t L = 0; L < alpha.size(); ++L)
    {
      alpha[L].resize(order + 1);
      beta[L].resize(order + 1);
      gamma[L].resize(order + 1);
      alpha2[L].resize(order + 1);

      // Set residues to zero at recursion_level==0
      const size_t max_pole_order(
        std::min<size_t>(order, L_max_recursion - L));
      alpha[L][0].resize(max_pole_order, 0);
      if(nu > 1)
        {
          beta[L][0].resize(std::min(max_pole_order / 2, size_t(nu) - 1), 0);
        }
      else
        {
          beta[L][0].resize(0);
        }
      // For third family k=1...2L+2nu except k=J+1...J+2nu-1.
      // For these excluded values we will simply keep the entries as 0.
      gamma[L][0].resize(std::min(max_pole_order, 2 * L + 2 * size_t(nu)), 0);
      alpha2[L][0].resize(
        std::max((int64_t(max_pole_order) - 2 * int64_t(nu + L)) / 2,
                 int64_t(0)),
        0);
    }
  

  // At the beginning, we have no data, so set all blocks to the same
  // cost.
  std::vector<uint64_t> timings(L_max_recursion, 1), new_timings(timings);
  for(int64_t recursion_level = 1; recursion_level <= order; ++recursion_level)
    {
      const std::string recursion_timer_name(
        timer_prefix + "recursion_" + std::to_string(recursion_level));
      Timer &recursion_timer(timers.add_and_start(recursion_timer_name));

      // Timer &thread_timer(
      //   timers.add_and_start(recursion_timer_name + ".thread_0"));
      // append_residues(h_infinity, order, nu, Delta_12, Delta_34,
      //                 L_max_recursion, recursion_level,
      //                 // num_threads, thread_rank,
      //                 1, 0,
      //                 timings, *this, new_timings, thread_timer);

      std::list<std::thread> threads;
      for(size_t thread_rank = 0; thread_rank < num_threads; ++thread_rank)
        {
          Timer &thread_timer(timers.add_and_start(
            recursion_timer_name + ".thread_" + std::to_string(thread_rank)));

          threads.emplace_back(append_residues_even_d, std::cref(h_infinity),
                               order, std::cref(nu), std::cref(Delta_12),
                               std::cref(Delta_34), L_max_recursion,
                               recursion_level, num_threads, thread_rank,
                               std::cref(timings), std::ref(*this),
                               std::ref(new_timings), std::ref(thread_timer));
        }
      for(auto &thread : threads)
        {
          thread.join();
        }
      recursion_timer.stop();
      std::swap(timings, new_timings);
    }
}