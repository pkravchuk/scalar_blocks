#include "Matrix.hxx"
#include "../../Bigfloat.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <vector>
#include <utility>

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
r_delta_matrices(const std::vector<std::pair<int64_t, int64_t>> &derivs,
                 const Bigfloat &r_crossing, const int64_t &n_max)
{
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result;
  for(auto &deriv : derivs)
    {
      Matrix<boost::math::tools::polynomial<Bigfloat>> deriv_result(2 * n_max,
                                                                    n_max);
      for(int64_t num_r_derivs = 0; num_r_derivs <= deriv.first;
          ++num_r_derivs)
        {
          boost::math::tools::polynomial<Bigfloat> delta_term({1});
          boost::math::tools::polynomial<Bigfloat> delta_poly({0, 1});
          const int polynomial_degree(deriv.first - num_r_derivs);
          deriv_result(num_r_derivs, deriv.second)
            = binomial_coefficient(deriv.first, num_r_derivs) * delta_term
              * pow(r_crossing, -polynomial_degree);

          for(int64_t degree = 1; degree <= polynomial_degree; ++degree)
            {
              delta_term *= delta_poly;
              delta_poly[0] -= 1;
            }
          deriv_result(num_r_derivs, deriv.second) *= delta_term;
        }
      // Flatten the result into a vector so that we do not store lots
      // of identically zero terms
      result.emplace_back();
      auto &result_back(result.back());
      for(auto &deriv_of_deriv : derivs)
        {
          result_back.push_back(
            deriv_result(deriv_of_deriv.first, deriv_of_deriv.second));
        }
    }
  return result;
}
