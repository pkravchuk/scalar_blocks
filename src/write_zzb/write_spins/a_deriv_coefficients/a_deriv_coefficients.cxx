#include "rho.hxx"
#include "../../../Bigfloat.hxx"

std::vector<Bigfloat> alpha_series(const int64_t &p, const int64_t &order);

namespace
{
  Bigfloat alpha(const int64_t &n, const int64_t &p)
  {
    return factorial(n) * (alpha_series(p, n).at(n));
  }
}

std::vector<Bigfloat> a_deriv_coefficients(const int64_t &n)
{
  std::vector<Bigfloat> result;
  result.reserve(n + 1);
  for(int64_t k = 0; k <= n; ++k)
    {
      Bigfloat prefactor((pow(2, -n) / factorial(k))
                         * pow(rho(Bigfloat(0.5)), k));
      Bigfloat sum(0);
      for(int64_t j = 0; j <= k; ++j)
        {
          sum += ((k - j) % 2 == 0 ? 1 : -1) * binomial_coefficient(k, j)
                 * alpha(n, 2 * j);
        }
      result.emplace_back(prefactor * sum);
    }
  return result;
}
