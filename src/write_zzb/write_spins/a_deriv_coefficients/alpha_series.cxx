#include "rho.hxx"
#include "../../../Sacado_Bigfloat.hxx"

template <typename T>
T lambda(const T& z)
{
  return sqrt(rho(z)/rho(Bigfloat(0.5)));
}
  
std::vector<Bigfloat> alpha_series(const int64_t &p, const int64_t &order)
{
  std::vector<Bigfloat> result;
  if(p==0)
    {
      result.emplace_back(1);
      result.resize(1+order,Bigfloat(0));
    }
  else
    {
      Sacado::Tay::Taylor<Bigfloat> z(order);
      z.fastAccessCoeff(0) = 0.5;
      z.fastAccessCoeff(1) = 1.0;
      Sacado::Tay::Taylor<Bigfloat> u = pow(lambda(z),p);
      std::copy(u.coeff(),u.coeff()+u.degree()+1,std::back_inserter(result));
    }
  return result;
}
