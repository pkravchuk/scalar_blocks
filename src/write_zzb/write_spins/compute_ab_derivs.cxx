#include "../../Bigfloat.hxx"
#include "../../Dimension/Dimension.hxx"

#include <boost/math/tools/polynomial.hpp>

namespace
{
  // This definition of CR() uses the opposite convention from the
  // Mathematica code, switching 'n' and 'm'.  The number of 'm'
  // elements depends on 'n', so we make 'n' come first in the
  // underlying storage.

  boost::math::tools::polynomial<Bigfloat>
  CR(const int64_t &n, const int64_t &m,
     const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
       &result)
  {
    return (n >= 0 && m >= 0 ? result.at(n).at(m)
                             : boost::math::tools::polynomial<Bigfloat>({0}));
  }
}

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
compute_ab_derivs(
  const Bigfloat &dim, const Bigfloat &Delta_12, const Bigfloat &Delta_34,
  const int64_t &n_max, const boost::math::tools::polynomial<Bigfloat> &lambda,
  const std::vector<boost::math::tools::polynomial<Bigfloat>> &a_derivs)
{
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result;
  result.resize(n_max);

  for(int64_t n = 0; n < static_cast<int64_t>(result.size()); ++n)
    {
      result[n].resize(2 * (n_max - n));

      for(int64_t m = 0; m < static_cast<int64_t>(result[n].size()); ++m)
        {
          if(n == 0)
            {
              result.at(n).at(m) = a_derivs.at(m);
            }
          else
            {
              result.at(n).at(m)
                = m * (2 - 3 * m + m * m) * CR(n, m - 3, result)

                  + (m - 1) * m * CR(n, m - 2, result)

                  - m * CR(n, m - 1, result)

                  + (m
                       * (20 + m * m + dim * (1 + m - 2 * n) - 30 * n
                          + 12 * n * n + 5 * Delta_12 - 4 * n * Delta_12
                          - 5 * Delta_34 + 4 * n * Delta_34
                          - Delta_12 * Delta_34
                          + m * (-15 + 12 * n - Delta_12 + Delta_34))
                       * CR(n - 1, m - 1, result)

                     + (2 + m * m - 6 * n + 4 * n * n + 2 * dim * (-1 + m + n)
                        + 4 * Delta_12 - 4 * n * Delta_12 - 4 * Delta_34
                        + 4 * n * Delta_34 - Delta_12 * Delta_34
                        + m * (-9 + 8 * n - 2 * Delta_12 + 2 * Delta_34)
                        + 2 * lambda)
                         * CR(n - 1, m, result)

                     + ((-1 + n)
                        * (-4 - dim + 3 * m + 4 * n - Delta_12 + Delta_34)
                        * CR(n - 2, m + 1, result))

                     + (4 + dim - m - 4 * n - Delta_12 + Delta_34)
                         * CR(n - 1, m + 1, result)

                     + (n - 1) * CR(n - 2, m + 2, result)

                     - CR(-1 + n, 2 + m, result))

                      / (2 * (-3 + dim + 2 * n));
            }
        }
    }
  return result;
}
