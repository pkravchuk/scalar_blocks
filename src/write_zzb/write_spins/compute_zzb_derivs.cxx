#include "../../Bigfloat.hxx"

#include <boost/math/tools/polynomial.hpp>

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
compute_zzb_derivs(
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &ab_derivs,
  const int64_t &n_max)
{
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result(
    2 * n_max);
  for(size_t j = 0; j < result.size(); ++j)
    {
      // Explicitly specify size_t to handle cases when size_t != uint64_t
      result[j].resize(std::min<size_t>(2 * n_max - j, j + 1), {0});

      for(size_t k = 0; k < result[j].size(); ++k)
        for(size_t n = 0; n <= (j + k) / 2; ++n)
          for(size_t m = 0; m + 2 * n <= j + k; ++m)
            {
              // Boost does not handle negative factorials.  The
              // result is complex infinity, which makes the whole
              // contribution zero.  So we special case them.
              if(j >= m && j + k >= (m + 2 * n) && j <= m + 2 * n)
                {
                  result[j][k]
                    += ab_derivs.at(n).at(j + k - 2 * n) * pow(-1, j - m)
                       * factorial(j) * factorial(k) * factorial(2 * n)
                       / (factorial(j - m) * factorial(m)
                          * factorial(j + k - m - 2 * n) * factorial(n)
                          * factorial(-j + m + 2 * n));
                }
            }
    }
  return result;
}
