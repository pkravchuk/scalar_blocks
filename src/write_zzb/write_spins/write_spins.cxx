#include "../../Bigfloat.hxx"
#include "../../Timer.hxx"
#include "../../Dimension/Dimension.hxx"

#include <boost/filesystem.hpp>
#include <boost/math/tools/polynomial.hpp>

#include <vector>
#include <set>

std::vector<Bigfloat> a_deriv_coefficients(const int64_t &n);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
compute_ab_derivs(
  const Bigfloat &dim, const Bigfloat &Delta_12, const Bigfloat &Delta_34,
  const int64_t &n_max, const boost::math::tools::polynomial<Bigfloat> &lambda,
  const std::vector<boost::math::tools::polynomial<Bigfloat>> &a_derivs);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
compute_zzb_derivs(
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &ab_derivs,
  const int64_t &n_max);

std::pair<std::vector<Bigfloat>, std::vector<Bigfloat>>
cb_poles(const Dimension &dim, const int64_t &L, const int64_t &order);

void write_spins(
  const boost::filesystem::path &nmax_dir, const Dimension &dim,
  const std::string &Delta_12_string, const std::string &Delta_34_string,
  const std::set<int64_t> &spins, const Bigfloat &Delta_12,
  const Bigfloat &Delta_34, const int64_t &n_max, const bool &output_ab,
  const int64_t &kept_pole_order, const int64_t &order,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &shifted_blocks,
  const size_t &num_threads, const size_t &thread_rank, const bool &debug,
  Timer &thread_timer)
{
  auto spin(spins.begin());
  std::advance(spin, std::min(spins.size(), thread_rank));
  while(spin != spins.end())
    {
      const int64_t L(*spin);
      std::stringstream ss;
      if(!output_ab)
        ss << "/zzbDerivTable-d";
      else
        ss << "/abDerivTable-d";

      ss << dim.str << "-delta12-" << Delta_12_string << "-delta34-"
         << Delta_34_string << "-L" << L << "-nmax" << n_max
         << "-keptPoleOrder" << kept_pole_order << "-order" << order << ".m";
      boost::filesystem::ofstream outfile(nmax_dir / ss.str());
      outfile << std::fixed;
      if(debug)
        {
          std::cout << "Writing: " << (nmax_dir / ss.str()).string() << "\n";
        }
      outfile.precision(boost::multiprecision::mpf_float::default_precision());
      outfile << "{";

      std::vector<boost::math::tools::polynomial<Bigfloat>> a_derivs(2 * n_max,
                                                                     {0});
      for(size_t n = 0; n < a_derivs.size(); ++n)
        {
          std::vector<Bigfloat> coefficients(a_deriv_coefficients(n));

          boost::math::tools::polynomial<Bigfloat> &a_deriv(a_derivs.at(n));
          for(size_t r_deriv = 0; r_deriv < coefficients.size(); ++r_deriv)
            {
              a_deriv
                += shifted_blocks.at(L).at(r_deriv) * coefficients.at(r_deriv);
            }
        }

      const boost::math::tools::polynomial<Bigfloat> x({0, 1});
      const boost::math::tools::polynomial<Bigfloat> lambda(
        L * (2 * dim.nu + L) + (-2 + L + x) * (2 * dim.nu + L + x));

      std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
        ab_derivs(compute_ab_derivs(2 * dim.nu + 2, Delta_12, Delta_34, n_max,
                                    lambda, a_derivs));

      if(!output_ab)
        {
          std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
            zzb_derivs(compute_zzb_derivs(ab_derivs, n_max));

          for(size_t z = 0; z < zzb_derivs.size(); ++z)
            {
              for(size_t zb = 0; zb < zzb_derivs.at(z).size(); ++zb)
                {
                  outfile << "zzbDeriv[" << z << "," << zb << "] -> ";

                  for(size_t power = 0; power < zzb_derivs.at(z).at(zb).size();
                      ++power)
                    {
                      outfile << zzb_derivs.at(z).at(zb)[power];
                      if(power != 0)
                        {
                          outfile << "*x^" << power;
                        }
                      if(power + 1 != zzb_derivs.at(z).at(zb).size())
                        {
                          outfile << "\n   + ";
                        }
                    }
                  if(zb + 1 != zzb_derivs.at(z).size())
                    {
                      outfile << ",\n ";
                    }
                }
              if(z + 1 != zzb_derivs.size())
                {
                  outfile << ",\n ";
                }
              else
                {
                  outfile << "}\n";
                }
            }
        }
      else
        {
          // We use max_degree, because the a,b derivatives contain
          // spurious leading x^n powers with coefficients that are pure
          // numerical error. Since we can compute the degree of the
          // polynomial, we can safely truncate them. In z,zb case this is not
          // an issue since the polynomials happen to have the correct degree.
          // This happens because at intermediate stages b derivatives produce
          // extra 2 powers of x, whereas in the end a and b derivatives both
          // produce only 1 power (since is a smooth function of b at b=0).
          // This leads to cancellations which result in these spurious leading
          // powers.
          //
          // Maybe we should clean these powers up at the stage where we
          // compute them...
          auto poles = cb_poles(dim, L, kept_pole_order);
          int64_t max_degree = poles.first.size() + 2 * poles.second.size();

          for(size_t b = 0; b < ab_derivs.size(); ++b)
            {
              for(size_t a = 0; a < ab_derivs.at(b).size(); ++a)
                {
                  outfile << "abDeriv[" << a << "," << b << "] -> ";

                  for(size_t power = 0; power < ab_derivs.at(b).at(a).size()
                                        && power <= max_degree + a + b;
                      ++power)
                    {
                      outfile << ab_derivs.at(b).at(a)[power];
                      if(power != 0)
                        {
                          outfile << "*x^" << power;
                        }
                      if(power + 1 != ab_derivs.at(b).at(a).size()
                         && power != max_degree + a + b)
                        {
                          outfile << "\n   + ";
                        }
                    }
                  if(a + 1 != ab_derivs.at(b).size())
                    {
                      outfile << ",\n ";
                    }
                }
              if(b + 1 != ab_derivs.size())
                {
                  outfile << ",\n ";
                }
              else
                {
                  outfile << "}\n";
                }
            }
        }
      for(size_t n = 0; n < num_threads && spin != spins.end(); ++n)
        {
          ++spin;
        }
    }
  thread_timer.stop();
}
