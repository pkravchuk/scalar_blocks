#include "../../Bigfloat.hxx"
#include "../../Dimension/Dimension.hxx"

#include <vector>

std::pair<std::vector<Bigfloat>, std::vector<Bigfloat>>
cb_poles(const Dimension &dim, const int64_t &L, const int64_t &order)
{
  std::vector<Bigfloat> result, result_double;
  if(!dim.even)
    {
      for(int64_t n = 1; n <= order; ++n)
        {
          result.push_back(-(n + L - 1));
        }
      for(int64_t k = 1; k <= order / 2; ++k)
        {
          result.push_back(-(-dim.nu + k - 1));
        }
      for(int64_t n = 1; n <= std::min(order, L); ++n)
        {
          result.push_back(-(-L - 2 * dim.nu + n - 1));
        }
    }
  else
    {
      for(int64_t k = 1; k <= order; ++k)
        {
          if(2 * dim.nu_int + 2 * L + 2 * k <= order)
            result_double.push_back(1 - L - k);
          else
            result.push_back(1 - L - k);
        }
      for(int k = 1; 2 * k <= order && k <= dim.nu_int - 1; ++k)
        {
          result.push_back(1 + dim.nu - k);
        }
      for(int k = 1; k <= order && k <= 2 * L + 2 * dim.nu_int; ++k)
        {
          if(k > L && k < L + 2 * dim.nu_int)
            continue;
          result.push_back(1 + 2 * dim.nu + L - k);
        }
    }
  return std::make_pair(result, result_double);
}

// std::vector<Bigfloat> cb_poles(const Bigfloat &nu,
//                                const int64_t &L,
//                                const int64_t &order)
// {
//   std::vector<Bigfloat> result;
//   for(int64_t n=1; n<=order; ++n)
//     {
//       result.push_back(-(n+L-1));
//     }
//   for(int64_t k=1; k<=order/2; ++k)
//     {
//       result.push_back(-(-nu+k-1));
//     }
//   for(int64_t n=1; n<=std::min(order,L); ++n)
//     {
//       result.push_back(-(-L-2*nu+n-1));
//     }
//   return result;
// }
