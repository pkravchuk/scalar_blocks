#include "../../Bigfloat.hxx"

#include <Eigen/LU>

#include <vector>

std::pair<std::vector<std::pair<Bigfloat, Bigfloat>>,
          std::vector<std::pair<Bigfloat, std::pair<Bigfloat, Bigfloat>>>>
shift_poles(
  const std::vector<std::pair<Bigfloat, Bigfloat>> &unprotected_poles,
  const std::vector<std::pair<Bigfloat, std::pair<Bigfloat, Bigfloat>>>
    &unprotected_double_poles,
  const std::vector<Bigfloat> &keep, const std::vector<Bigfloat> &keep_double)
{
  using Eigen_Matrix = Eigen::Matrix<Bigfloat, Eigen::Dynamic, Eigen::Dynamic>;
  using Eigen_Vector = Eigen::Matrix<Bigfloat, Eigen::Dynamic, 1>;
  const int64_t size(keep.size() + 2 * keep_double.size()),
    keep_size(keep.size()), keep_double_size(keep_double.size());
  Eigen_Matrix A(size, size);

  for(int64_t row = 0; row < size; ++row)
    {
      int64_t m(row - (size / 2));
      for(int64_t column = 0; column < keep_size; ++column)
        {
          A(row, column) = pow(keep[column], m);
        }
      for(int64_t column = keep_size; column + 1 < size; column += 2)
        {
          A(row, column) = pow(keep_double[(column - keep_size) / 2], m);
          A(row, column + 1)
            = m * pow(keep_double[(column - keep_size) / 2], m - 1);
        }
    }
  Eigen_Vector b(size);
  for(int64_t row = 0; row < size; ++row)
    {
      int64_t m(row - (size / 2));
      b(row) = 0;
      for(auto &pole : unprotected_poles)
        {
          b(row) += pole.second * pow(pole.first, m);
        }
      for(auto &pole : unprotected_double_poles)
        {
          Bigfloat power = pow(pole.first, m - 1);
          b(row) += pole.second.first * pole.first * power;
          b(row) += m * pole.second.second * power;
        }
    }

  Eigen_Vector x = A.lu().solve(b);
  std::vector<std::pair<Bigfloat, Bigfloat>> result;
  std::vector<std::pair<Bigfloat, std::pair<Bigfloat, Bigfloat>>> result_double;
  result.reserve(keep_size);
  result_double.reserve(keep_double_size);
  for(int64_t row = 0; row < keep_size; ++row)
    {
      result.emplace_back(keep[row], x(row));
    }
  for(int64_t row = 0; row < keep_double_size; ++row)
    {
      result_double.emplace_back(
        keep_double[row],
        std::make_pair(x(2 * row + keep_size), x(2 * row + 1 + keep_size)));
    }
  return std::make_pair(result, result_double);
}
