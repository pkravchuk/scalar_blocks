#include "../Delta_Fraction.hxx"

// boost::math::tools::polynomial<Bigfloat> together_with_factors(
//   const std::vector<std::pair<Bigfloat, Bigfloat>> &all_kept_poles,
//   const boost::math::tools::polynomial<Bigfloat> &polynomial)
// {
//   boost::math::tools::polynomial<Bigfloat> product(polynomial);
//   for(auto &pole : all_kept_poles)
//     {
//       product *= boost::math::tools::polynomial<Bigfloat>({-pole.first, 1});
//     }

//   boost::math::tools::polynomial<Bigfloat> sum;
//   for(size_t index(0); index < all_kept_poles.size(); ++index)
//     {
//       boost::math::tools::polynomial<Bigfloat> complement(
//         all_kept_poles[index].second);
//       for(size_t complement_factor(0);
//           complement_factor < all_kept_poles.size(); ++complement_factor)
//         {
//           if(complement_factor != index)
//             {
//               complement *= boost::math::tools::polynomial<Bigfloat>({
//                   -all_kept_poles[complement_factor].first, 1});
//             }
//         }
//       sum+=complement;
//     }
//   return product + sum;
// }

boost::math::tools::polynomial<Bigfloat> together_with_factors(
  const std::vector<std::pair<Bigfloat, Bigfloat>> &all_kept_poles,
  const std::vector<std::pair<Bigfloat, std::pair<Bigfloat, Bigfloat>>>
    &all_kept_double_poles,
  const boost::math::tools::polynomial<Bigfloat> &polynomial)
{
  boost::math::tools::polynomial<Bigfloat> product(polynomial);
  for(auto &pole : all_kept_poles)
    {
      product *= boost::math::tools::polynomial<Bigfloat>({-pole.first, 1});
    }
  for(auto &pole : all_kept_double_poles)
    {
      product *= boost::math::tools::polynomial<Bigfloat>(
        {pole.first * pole.first, -2 * pole.first, 1});
    }

  boost::math::tools::polynomial<Bigfloat> sum;
  for(size_t index(0); index < all_kept_poles.size(); ++index)
    {
      boost::math::tools::polynomial<Bigfloat> complement(
        all_kept_poles[index].second);
      for(size_t complement_factor(0);
          complement_factor < all_kept_poles.size(); ++complement_factor)
        {
          if(complement_factor != index)
            {
              complement *= boost::math::tools::polynomial<Bigfloat>(
                {-all_kept_poles[complement_factor].first, 1});
            }
        }
      for(size_t complement_factor(0);
          complement_factor < all_kept_double_poles.size();
          ++complement_factor)
        {
          complement *= boost::math::tools::polynomial<Bigfloat>(
            {pow(all_kept_double_poles[complement_factor].first, 2),
             -2 * all_kept_double_poles[complement_factor].first, 1});
        }
      sum += complement;
    }
  for(size_t index(0); index < all_kept_double_poles.size(); ++index)
    {
      boost::math::tools::polynomial<Bigfloat> complement(
        {-all_kept_double_poles[index].second.first
             * all_kept_double_poles[index].first
           + all_kept_double_poles[index].second.second,
         all_kept_double_poles[index].second.first});
      for(size_t complement_factor(0);
          complement_factor < all_kept_poles.size(); ++complement_factor)
        {
          complement *= boost::math::tools::polynomial<Bigfloat>(
            {-all_kept_poles[complement_factor].first, 1});
        }
      for(size_t complement_factor(0);
          complement_factor < all_kept_double_poles.size();
          ++complement_factor)
        {
          if(complement_factor != index)
            {
              complement *= boost::math::tools::polynomial<Bigfloat>(
                {pow(all_kept_double_poles[complement_factor].first, 2),
                 -2 * all_kept_double_poles[complement_factor].first, 1});
            }
        }
      sum += complement;
    }
  return product + sum;
}
