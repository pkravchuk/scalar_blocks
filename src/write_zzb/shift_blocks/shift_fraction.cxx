#include "../Delta_Fraction.hxx"

Delta_Fraction
shift_fraction(const Delta_Fraction &fraction, const Bigfloat &offset)
{
  Delta_Fraction result;
  boost::math::tools::polynomial<Bigfloat> offset_atom({offset, 1});
  boost::math::tools::polynomial<Bigfloat> offset_power({1});
  for(size_t power(0); power < fraction.polynomial.size(); ++power)
    {
      result.polynomial += fraction.polynomial[power] * offset_power;
      offset_power *= offset_atom;
    }

  result.residues.reserve(fraction.residues.size());
  result.double_residues.reserve(fraction.double_residues.size());
  for(auto &residue : fraction.residues)
    {
      result.residues.emplace_back(residue.first - offset, residue.second);
    }
  for(auto &residue : fraction.double_residues)
    {
      result.double_residues.emplace_back(residue.first - offset, residue.second);
    }
  return result;
}
