#include "../Delta_Fraction.hxx"
#include "../../Timers.hxx"
#include "../../Dimension/Dimension.hxx"

#include <list>
#include <thread>

// need to check that this works for fractional dimenions...
inline bool
is_protected(const Bigfloat &p, const int64_t &L, const Bigfloat &nu)
{
  return abs(p - (L == 0 ? -nu : Bigfloat(0))) <= min(nu / 2, Bigfloat(0.2));
}

std::pair<std::vector<Bigfloat>, std::vector<Bigfloat>>
cb_poles(const Dimension &dim, const int64_t &L, const int64_t &order);

Delta_Fraction
shift_fraction(const Delta_Fraction &fraction, const Bigfloat &offset);

std::pair<std::vector<std::pair<Bigfloat, Bigfloat>>,
          std::vector<std::pair<Bigfloat, std::pair<Bigfloat, Bigfloat>>>>
shift_poles(
  const std::vector<std::pair<Bigfloat, Bigfloat>> &unprotected_poles,
  const std::vector<std::pair<Bigfloat, std::pair<Bigfloat, Bigfloat>>>
    &unprotected_double_poles,
  const std::vector<Bigfloat> &keep, const std::vector<Bigfloat> &keep_double);

boost::math::tools::polynomial<Bigfloat> together_with_factors(
  const std::vector<std::pair<Bigfloat, Bigfloat>> &all_kept_poles,
  const std::vector<std::pair<Bigfloat, std::pair<Bigfloat, Bigfloat>>>
    &all_kept_double_poles,
  const boost::math::tools::polynomial<Bigfloat> &polynomial);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
shift_blocks(const std::vector<std::vector<Delta_Fraction>> &blocks,
             const Dimension &dim, const int64_t &kept_pole_order,
             const size_t &num_threads, const std::string &timer_prefix,
             Timers &timers)
{
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result(
    blocks.size());

  std::list<std::thread> threads;
  for(size_t thread_rank = 0; thread_rank < num_threads; ++thread_rank)
    {
      Timer &thread_timer(timers.add_and_start(timer_prefix + "thread_"
                                               + std::to_string(thread_rank)));
      threads.emplace_back(
        [&](const size_t &thread_rank, Timer &timer) {
          for(size_t L = thread_rank; L < blocks.size(); L += num_threads)
            {
              std::vector<Bigfloat> keep, keep_double, cb_poles_single,
                cb_poles_double;
              std::tie(cb_poles_single, cb_poles_double)
                = cb_poles(dim, L, kept_pole_order);
              for(auto &pole : cb_poles_single)
                {
                  Bigfloat pole_to_keep(pole - (L + 2 * dim.nu));
                  if(!is_protected(pole_to_keep, L, dim.nu))
                    {
                      keep.push_back(pole_to_keep);
                    }
                }
              for(auto &pole : cb_poles_double)
                {
                  Bigfloat pole_to_keep(pole - (L + 2 * dim.nu));
                  if(!is_protected(pole_to_keep, L, dim.nu))
                    {
                      keep_double.push_back(pole_to_keep);
                    }
                }
              for(auto &fraction : blocks[L])
                {
                  const Delta_Fraction shifted(
                    shift_fraction(fraction, L + 2 * dim.nu));
                  std::vector<std::pair<Bigfloat, Bigfloat>> all_kept_poles,
                    unprotected_poles;
                  std::vector<std::pair<Bigfloat, std::pair<Bigfloat, Bigfloat>>>
                    all_kept_double_poles, unprotected_double_poles;

                  for(auto &residue : shifted.residues)
                    {
                      if(is_protected(residue.first, L, dim.nu))
                        {
                          all_kept_poles.push_back(residue);
                        }
                      else
                        {
                          unprotected_poles.push_back(residue);
                        }
                    }
                  for(auto &residue : shifted.double_residues)
                    {
                      if(is_protected(residue.first, L, dim.nu))
                        {
                          all_kept_double_poles.push_back(residue);
                        }
                      else
                        {
                          unprotected_double_poles.push_back(residue);
                        }
                    }

                  std::vector<std::pair<Bigfloat, Bigfloat>> shifted_poles;
                  std::vector<std::pair<Bigfloat, std::pair<Bigfloat, Bigfloat>>>
                    shifted_double_poles;

                  std::tie(shifted_poles, shifted_double_poles)
                    = shift_poles(unprotected_poles, unprotected_double_poles,
                                  keep, keep_double);
                  // shifted_poles = unprotected_poles;
                  // shifted_double_poles = unprotected_double_poles;
                  for(auto &pole : shifted_poles)
                    {
                      all_kept_poles.push_back(pole);
                    }
                  for(auto &pole : shifted_double_poles)
                    {
                      all_kept_double_poles.push_back(pole);
                    }
                  result[L].push_back(together_with_factors(
                    all_kept_poles, all_kept_double_poles,
                    shifted.polynomial));
                }
              timer.stop();
            }
        },
        thread_rank, std::ref(thread_timer));
    }
  for(auto &thread : threads)
    {
      thread.join();
    }

  return result;
}
