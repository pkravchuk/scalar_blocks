#include "Delta_Fraction.hxx"
#include "../mn_deriv_set.hxx"
#include "../Timers.hxx"
#include "../Dimension/Dimension.hxx"

#include <boost/filesystem.hpp>

#include <list>
#include <set>
#include <thread>

std::vector<std::vector<Delta_Fraction>>
compute_blocks(const std::vector<std::pair<int64_t, int64_t>> &derivs,
               const int64_t &L_max, const Dimension &dim,
               const Bigfloat &Delta_12, const Bigfloat &Delta_34,
               const int64_t &n_max, const int64_t &order,
               const size_t &num_threads, const std::string &timer_prefix,
               Timers &timers);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
shift_blocks(const std::vector<std::vector<Delta_Fraction>> &blocks,
             const Dimension &dim, const int64_t &kept_pole_order,
             const size_t &num_threads, const std::string &timer_prefix,
             Timers &timers);

void write_spins(
  const boost::filesystem::path &output_dir, const Dimension &dim,
  const std::string &Delta_12_string, const std::string &Delta_34_string,
  const std::set<int64_t> &spins, const Bigfloat &Delta_12,
  const Bigfloat &Delta_34, const int64_t &n_max, const bool &output_ab,
  const int64_t &kept_pole_order, const int64_t &order,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &shifted_blocks,
  const size_t &num_threads, const size_t &thread_rank, const bool &debug,
  Timer &thread_timer);

void write_zzb(const boost::filesystem::path &output_dir, const Dimension &dim,
               const std::string &Delta_12_string,
               const std::string &Delta_34_string,
               const std::pair<std::set<int64_t>, int64_t> &spins,
               const int64_t &n_max, const bool &output_ab,
               const std::vector<int64_t> &kept_pole_orders,
               const int64_t &order, const size_t &num_threads,
               const bool &debug, Timers &timers)
{
  std::vector<std::pair<int64_t, int64_t>> derivs(mn_deriv_set(n_max));
  Timer &compute_blocks_timer(timers.add_and_start("compute_blocks"));
  const Bigfloat Delta_12(Delta_12_string), Delta_34(Delta_34_string);
  std::vector<std::vector<Delta_Fraction>> blocks(
    compute_blocks(derivs, spins.second, dim, Delta_12, Delta_34, n_max, order,
                   num_threads, "compute_blocks.", timers));
  compute_blocks_timer.stop();

  create_directories(output_dir);

  for(auto &kept_pole_order : kept_pole_orders)
    {
      const std::string kept_pole_timer_name(
        "kept_pole_" + std::to_string(kept_pole_order));
      Timer &kept_pole_timer(timers.add_and_start(kept_pole_timer_name));

      const std::string shift_timer_name(kept_pole_timer_name + ".shift");
      Timer &shift_timer(timers.add_and_start(shift_timer_name));

      std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
        shifted_blocks(shift_blocks(blocks, dim, kept_pole_order, num_threads,
                                    shift_timer_name + ".", timers));
      shift_timer.stop();

      std::list<std::thread> threads;
      for(size_t thread_rank = 0; thread_rank < num_threads; ++thread_rank)
        {
          Timer &thread_timer(timers.add_and_start(
            kept_pole_timer_name + ".thread_" + std::to_string(thread_rank)));
          threads.emplace_back(
            write_spins, std::cref(output_dir), dim,
            std::cref(Delta_12_string), std::cref(Delta_34_string),
            spins.first, std::cref(Delta_12), std::cref(Delta_34), n_max,
            output_ab, kept_pole_order, order, std::cref(shifted_blocks),
            num_threads, thread_rank, debug, std::ref(thread_timer));
        }
      for(auto &thread : threads)
        {
          thread.join();
        }
      kept_pole_timer.stop();
    }
}
