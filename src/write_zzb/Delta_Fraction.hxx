#pragma once

#include "../Bigfloat.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <utility>
#include <vector>

struct Delta_Fraction
{
  boost::math::tools::polynomial<Bigfloat> polynomial;
  std::vector<std::pair<Bigfloat, Bigfloat>> residues;
  std::vector<std::pair<Bigfloat, std::pair<Bigfloat, Bigfloat>>>
    double_residues;
};
