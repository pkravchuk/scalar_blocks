#include <boost/algorithm/string.hpp>

#include <set>
#include <vector>
#include <string>

std::pair<std::set<int64_t>, int64_t>
parse_spin_ranges(const std::string &spin_ranges_string)
{
  std::pair<std::set<int64_t>, int64_t> result;
  std::vector<std::string> spin_range_elements;
  boost::split(spin_range_elements, spin_ranges_string, boost::is_any_of(","));
  for(auto &spin_range : spin_range_elements)
    {
      std::vector<std::string> spin_numbers;
      boost::split(spin_numbers, spin_range, boost::is_any_of("-"));
      if(spin_numbers.empty())
        {
          throw std::runtime_error("Invalid spin-range.  It is empty or "
                                   "composed entirely of dashes: '"
                                   + spin_range + "'");
        }
      if(spin_numbers.size() == 1)
        {
          result.first.insert(std::stoi(spin_numbers.front()));
        }
      else if(spin_numbers.size() == 2)
        {
          int64_t current(std::stoi(spin_numbers.front())),
            finish(std::stoi(spin_numbers.back()));
          if(finish < current)
            {
              throw std::runtime_error(
                "Invalid range.  The number after the dash must be larger "
                "than the number before the dash: '"
                + spin_range + "'");
            }
          for(; current <= finish; ++current)
            {
              result.first.insert(current);
            }
        }
      else
        {
          throw std::runtime_error("Invalid spin-range.  It is empty or "
                                   "composed entirely of dashes: '"
                                   + spin_range + "'");
        }
    }
  if(result.first.empty())
    {
      throw std::runtime_error("Invalid spin-range.  It is empty: '"
                               + spin_ranges_string + "'");
    }
  result.second = *std::max_element(result.first.begin(), result.first.end());
  return result;
}
