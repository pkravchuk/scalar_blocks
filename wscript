import os

def options(opt):
    opt.load(['compiler_cxx','gnu_dirs','cxx14','boost','gmp','trilinos','eigen','threads'])

def configure(conf):
    conf.load(['compiler_cxx','gnu_dirs','cxx14','boost','gmp','trilinos','eigen','threads'])

def build(bld):
    default_flags=['-Wall', '-Wextra', '-O3']
    # default_flags=['-Wall', '-Wextra', '-g']
    use_packages=['cxx14','boost','gmp','trilinos','eigen','threads']
    
    # Main executable
    bld.program(source=['src/main.cxx',
                        'src/parse_spin_ranges.cxx',
                        'src/Dimension/Dimension.cxx',
                        'src/mn_deriv_set.cxx',
                        'src/write_zzb/write_zzb.cxx',
                        'src/write_zzb/write_spins/compute_ab_derivs.cxx',
                        'src/write_zzb/write_spins/a_deriv_coefficients/a_deriv_coefficients.cxx',
                        'src/write_zzb/write_spins/a_deriv_coefficients/alpha_series.cxx',
                        'src/write_zzb/write_spins/write_spins.cxx',
                        'src/write_zzb/write_spins/compute_zzb_derivs.cxx',
                        'src/write_zzb/compute_blocks/compute_blocks.cxx',
                        'src/write_zzb/compute_blocks/fill_result/fill_result.cxx',
                        'src/write_zzb/compute_blocks/fill_result/fill_result_even_d.cxx',
                        'src/write_zzb/compute_blocks/fill_result/restore_r_delta.cxx',
                        'src/write_zzb/compute_blocks/r_delta_matrices.cxx',
                        'src/write_zzb/compute_blocks/compute_h_infinity.cxx',
                        'src/write_zzb/compute_blocks/Residues/Residues/Residues.cxx',
                        'src/write_zzb/compute_blocks/Residues/Residues/append_residues/append_residues.cxx',
                        'src/write_zzb/compute_blocks/Residues/Residues/append_residues/append_residues_even_d.cxx',
                        'src/write_zzb/compute_blocks/Residues/Residues/append_residues/h_dot.cxx',
                        'src/write_zzb/compute_blocks/Residues/Residues/append_residues/h_even_d.cxx',
                        'src/write_zzb/shift_blocks/shift_blocks.cxx',
                        'src/write_zzb/shift_blocks/cb_poles.cxx',
                        'src/write_zzb/shift_blocks/shift_fraction.cxx',
                        'src/write_zzb/shift_blocks/shift_poles.cxx',
                        'src/write_zzb/shift_blocks/together_with_factors.cxx'],
                target='scalar_blocks',
                cxxflags=default_flags,
                use=use_packages
                )
