C++ re-implementation of scalar blocks code.

Installation instructions are detailed in [Install.md](Install.md).

Mathematical details about the computed blocks are described in [Conventions.pdf](conventions/conventions.pdf).

Some technical notes on the behavior of the program are collected in [Behavior.md](Behavior.md)

Running the executable with the `--help` option should print out all
of the options.  A simple example that uses 4 threads and should
finish quickly is
   
    ./build/scalar_blocks --dim 3 --order 5 --max-derivs 6 --spin-ranges 0-21 --poles 6 --delta-12 0.8791  --delta-34 0.8791 --num-threads=4 -o output --precision=665

The results will be in the directory `output/`.

`scalar_blocks` is also included in the [SDPB docker
image](https://hub.docker.com/r/wlandry/sdpb).  To use the docker
image, you need to map your local directory into a place in the docker
image.  A good default is `/usr/local/share/scalar_blocks`.  Mapping
your current directory onto `/usr/local/share/scalar_blocks` is
done with the `-v` option

    -v $PWD:/usr/local/share/scalar_blocks

The full command line then is

    docker run -v $PWD:/usr/local/share/scalar_blocks wlandry/sdpb:2.1.1 scalar_blocks --dim 3 --order 5 --max-derivs 6 --spin-ranges 0-21 --poles 6 --delta-12 0.8791  --delta-34 0.8791 --num-threads=4 -o /usr/local/share/scalar_blocks/output --precision=665

which will write the output into `output`.  Note that the files may be
owned by root.
