#! /usr/bin/env python
# encoding: utf-8

import os

def configure(conf):
    def get_param(varname,default):
        return getattr(Options.options,varname,'')or default

    if not conf.options.trilinos_incdir:
        for d in ['TRILINOS_INCLUDE','TRILINOS_INCLUDE_DIR','TRILINOS_INC_DIR']:
            env_dir=os.getenv(d)
            if env_dir:
                conf.options.trilinos_incdir=env_dir
                
    if not conf.options.trilinos_libdir:
        for d in ['TRILINOS_LIB','TRILINOS_LIB_DIR']:
            env_dir=os.getenv(d)
            if env_dir:
                conf.options.trilinos_incdir=env_dir

    if not conf.options.trilinos_dir:
        env_dir=os.getenv('TRILINOS_DIR')
        if env_dir:
            conf.options.trilinos_incdir=env_dir
                
    # Find TRILINOS
    if conf.options.trilinos_dir:
        if not conf.options.trilinos_incdir:
            conf.options.trilinos_incdir=conf.options.trilinos_dir + "/include"
        if not conf.options.trilinos_libdir:
            conf.options.trilinos_libdir=conf.options.trilinos_dir + "/lib"

    if conf.options.trilinos_incdir:
        trilinos_incdir=conf.options.trilinos_incdir.split()
    else:
        trilinos_incdir=['/usr/include/trilinos']

    if conf.options.trilinos_libdir:
        trilinos_libdir=conf.options.trilinos_libdir.split()
    else:
        trilinos_libdir=[]

    if conf.options.trilinos_libs:
        trilinos_libs_list=[conf.options.trilinos_libs.split()]
    else:
        trilinos_libs_list=[['sacado'],['trilinos_teuchoscomm','trilinos_teuchoscore','trilinos_kokkoscore']]

    found_trilinos=False
    for trilinos_libs in trilinos_libs_list:
        try:
            conf.check_cxx(msg='Checking for Trilinos with libs=' + str(trilinos_libs),
                           header_name='Sacado_No_Kokkos.hpp',
                           includes=trilinos_incdir,
                           uselib_store='trilinos',
                           libpath=trilinos_libdir,
                           rpath=trilinos_libdir,
                           lib=trilinos_libs,
                           use=['cxx14'])
        except conf.errors.ConfigurationError:
            continue
        else:
            found_trilinos=True
            break
    if not found_trilinos:
        conf.fatal('Could not find find Trilinos')
            

def options(opt):
    trilinos=opt.add_option_group('TRILINOS Options')
    trilinos.add_option('--trilinos-dir',
                   help='Base directory where trilinos is installed')
    trilinos.add_option('--trilinos-incdir',
                   help='Directory where trilinos include files are installed')
    trilinos.add_option('--trilinos-libdir',
                   help='Directory where trilinos library files are installed')
    trilinos.add_option('--trilinos-libs',
                   help='Names of the trilinos libraries without prefix or suffix\n'
                   '(e.g. "sacado")')
