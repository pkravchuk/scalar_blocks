\documentclass[11pt, oneside]{article}   	

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{amsbsy}
\usepackage{array}
\usepackage[linesnumbered,lined,boxed,commentsnumbered]{algorithm2e}

\usepackage{mathtools}
\usepackage[usenames,dvipsnames]{xcolor}

\usepackage{subcaption}

\usepackage{dsdshorthand}


\makeatletter
\def\@fpheader{\ }
\makeatother

\title{Scalar conformal blocks}
\date{}


\begin{document}

\maketitle

\section{Conventions for the blocks}

Scalar conformal blocks computed by \verb|scalar_blocks| are functions
\be
	G^{\De_{12},\De_{34}}_{\De,J}(z,\bar z),
\ee
where $\De_{ij}=\De_i-\De_j$ are the differences between the external scaling dimensions, and $\De$ and $J$ are the dimension and spin of the exchanged operator. These blocks are normalized so that
\be
	G^{\De_{12},\De_{34}}_{\De,J}(z,z)\sim r^{\De}\sim 4^{-\De}z^{\De}.
\ee
for $z\ll 1$, where
\be
	r=\frac{z}{(1+\sqrt{1-z})^2}
\ee
is the usual radial coordinate.

The code computes approximations to the derivatives
\be
	\cD^{\De_{12},\De_{34}}_{\De,J;m,n}\equiv {\ptl_z^m \ptl_{\bar z}^n G^{\De_{12},\De_{34}}_{\De,J}(z,\bar z)}\Big\vert_{z,\bar z=\half},
\ee
in the following form
\be
	\cD^{\De_{12},\De_{34}}_{\De,J;m,n}\approx\frac{r_0^{\De}}{Q_{\kappa,J}(\De)} P^{\De_{12},\De_{34}}_{J,\kappa,N;m,n}(x).
\ee
Here $r_0=3-2\sqrt 2$ is the value of $r$ corresponding to $z=\bar z=\half$, $Q_{\kappa,J}(\De)$ are simple polynomials to be described below, $x$ is defined as
\be
	x=\De-(J+d-2),
\ee
and the polynomials $P^{\De_{12},\De_{34}}_{J,\kappa,N;m,n}(x)$ is what the code actually outputs. Finally, $\kappa$ and $N$ are approximation parameters to be described below.

The output files contain a list of entries of the form
\begin{center}
	\verb|{..., zzbDeriv[m,n] -> |$P^{\De_{12},\De_{34}}_{J,\kappa,N;m,n}(x)$\verb|, ...}|,
\end{center}
where $m+n\leq \Lambda = 2 n_{max}-1$ and $m\geq n$. Here $n_{max}$ is the parameter supplied through \verb|--max-derivs|. The first condition is simply a cutoff on the number of computed derivatives, while the second condition is due to the symmetry
\be
P^{\De_{12},\De_{34}}_{J,\kappa,N;m,n}(x)=P^{\De_{12},\De_{34}}_{J,\kappa,N;n,m}(x).
\ee

When \verb|--output_ab| is set, then the output file contains 
\begin{center}
	\verb|{..., abDeriv[m,n] -> ..., ...}|
\end{center} 
and instead of $\ptl_z^m\ptl_{\bar z}^n$ the code outputs $\ptl_a^m \ptl_b^n$ derivatives with $m+2n\leq 2n_{max}-1$, where
\be
	a=z+\bar z,\quad b=(z-\bar z)^2.
\ee

\section{Approximation details}

The code computes the blocks using the following procedure. First, the $r$-expansion of the blocks on the diagonal $z=\bar z$ is computed, up to order $N$ which is supplied by the user through parameter \verb|--order|. This expansion is then used to determine
the derivatives along the diagonal $z=\bar z$ near $z=\bar z=\half$, up to order $\Lambda=2n_{max}-1$, where $n_{max}$ is \verb|--max-derivs|. 

Then the approximation is simplified by only keeping the poles in Zamolodchikov recursion relation whose residues start with $r^{n}$ with $n\leq\kappa$. The list of truncation orders $\kappa$ is supplied through \verb|--orders|. The other poles are not discarded, but instead their contribution is approximated by adjusting the residues of the kept poles (``pole-shifting''). The poles at unitarity bound are never discarded nor are their residues modified.

Finally, the off-diagonal derivatives are computed using the quadratic Casimir equation.

The polynomials $Q_{\kappa,J}$ are basically the common denominator obtained from the poles remaining after the pole-shifting procedure. It has different form depending on whether spacetime dimension $d$ (supplied through \verb|--dim|) is generic real, or an even integer. In the former case we have
\be
	Q_{\kappa,J}(\Delta)=\prod_{\De_*\in \Pi_{\kappa,J}^1}(\Delta-\Delta_*),
\ee
while in the latter case we have
\be
	Q_{\kappa,J}(\Delta)=\prod_{\De_*\in \Pi_{\kappa,J}^1}(\Delta-\Delta_*)\prod_{\De_*\in \Pi_{\kappa,J}^2}(\Delta-\Delta_*)^2.
\ee
The sets of poles $\Pi^i_{\kappa,J}$ are described below.

\subsection{Poles in generic number of dimensions}

In generic number of dimensions we have\footnote{For simplicity we assume that $\kappa\geq 2$. In practice one mostly uses $\kappa\geq 10$.}
\be
	\Pi^1_{\kappa,J}=&\{1-J-k\}_{k=1}^\kappa\nn\\& \cup \{1+\nu-k\}_{k=1}^{\lfloor{\kappa/2}\rfloor} \nn\\&\cup \{1+2\nu+J-k\}_{k=1}^{\min(\kappa,J)},
\ee
where $\nu=\frac{d-2}{2}$.

\subsection{Poles in even integer dimensions}

For positive even integer $d$ we have that $\nu=\frac{d-2}{2}$ is a non-negative integer. We then have the following sets of poles
\be
	\Pi^1_{\kappa,J}=&\{1-J-k\}_{k=\max(1,\lfloor \frac{\kappa - 2J-2\nu}{2}\rfloor+1)}^\kappa\nn\\& \cup \{1+\nu-k\}_{k=1}^{\min(\lfloor{\kappa/2}\rfloor,\nu-1)} \nn\\&\cup \{1+2\nu+J-k\}_{k=1}^{\min(\kappa,J)}
	\cup \{1+2\nu+J-k\}_{k=J+2\nu}^{\min(\kappa,2J+2\nu)},\\
	\Pi^2_{\kappa,J}=&\{1-J-k\}_{k=1}^{\lfloor \frac{\kappa - 2J-2\nu}{2}\rfloor}.
\ee

\section{Attribution}

The code of \verb|scalar_blocks| is based on Zamolodchikov-like recursion relations which were introduced in the context of higher-dimensional blocks in~\cite{Kos:2013tga, Kos:2014bka}. 
The specialization of these relations to even dimensions was derived in~\cite{KravchukUnpublished}. The pole-shifting procedure was described in~\cite{Kos:2013tga}.  The recursion relation relating off-diagonal to diagonal derivatives through Casimir equation was introduced in~\cite{ElShowk:2012ht}.

\bibliographystyle{JHEP}
\bibliography{refs}

\end{document}  