
* [Requirements](#requirements)
* [Installation](#installation)

# Requirements

Scalar Blocks requires

- A modern C++ compiler with C++ 14 support.  SDPB has been tested with
  GCC 6.4.0, 7.1.0, 7.3.0 and 8.2.0.

- [Boost C++ Libraries](http://www.boost.org/) Please be sure that the
  boost library is compiled with the same C++ compiler you are using.

- [The GNU Multiprecision Library](https://gmplib.org/) This is used
  by the Boost multiprecision library.  Only the C bindings are
  required.

- [Eigen](https://http://eigen.tuxfamily.org)

- [Python](https://python.org)

- [CMake](https://cmake.org/)

- [Trilinos](https://trilinos.org/) Scalar blocks only uses
  [Sacado](https://trilinos.org/packages/sacado/) for automatic
  differentiation.

Scalar Blocks has only been tested on Linux (Debian buster and Centos
7).  In principle, it should be installable on Mac OS X using 
libraries from a package manager such as [Homebrew](https://brew.sh).

Some of these dependencies may be available via modules or a package
manager.  On Yale's Grace cluster:

    module load Langs/GCC/7.3.0 Libs/Boost Tools/Cmake Libs/GMP Libs/Eigen

On Caltech's central HPC cluster:

    module load cmake/3.10.2 gcc/7.3.0 boost/1_68_0-gcc730 eigen/eigen

On Debian Buster:

    apt-get install libeigen3-dev libtrilinos-sacado-dev libtrilinos-sacado-dev libtrilinos-kokkos-dev libtrilinos-teuchos-dev libboost-dev

# Installation

1. Download the latest release of Trilinos from git

        git clone --branch trilinos-release-12-12-branch https://github.com/trilinos/Trilinos.git
    
2. Make the build directory and cd into it.

        mkdir -p Trilinos/build
        cd Trilinos/build
    
3. Configure Trilinos.  We only need Sacado, so we turn off everything else.

        cmake -DTrilinos_ENABLE_Sacado=ON -DTrilinos_ENABLE_Kokkos=OFF -DTrilinos_ENABLE_Teuchos=OFF -DCMAKE_INSTALL_PREFIX=../../trilinos_bin ..

4. Build and install Trilinos

        make && make install

5. Download Scalar Blocks

        cd ../..
        git clone https://gitlab.com/bootstrapcollaboration/scalar_blocks.git
        cd scalar_blocks

6. Configure the project using the included version of
   [waf](https://waf.io).  Waf can usually find libraries that are in
   system directories, but it needs direction for everything else is.
   If you are having problems, running `python ./waf --help` will give
   you a list of options.
   
   On Yale's Grace cluster, a working commands is

        ./waf configure --trilinos-dir=$HOME/project/trilinos_bin

    On Caltech's central HPC cluster:

        ./waf configure --trilinos-dir=$HOME/scalar_blocks/trilinos_bin --eigen-incdir=/software/eigen-b3f3d4950030/

    and on Debian buster, it is

        CXX=mpicxx python ./waf configure

7. Type `python ./waf` to build the executable in `build/scalar_blocks`.  Running
   
        ./build/scalar_blocks --help
   
   should give you a usage message.
